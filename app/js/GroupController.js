/*
	1. Group controller: line 5 to 310
	2. Group Detail controller: line 313 to end
*/

//==========GROUP CONTROLLER==========

function GroupController($scope,$rootScope) {
	$scope.select_2 = "";
	$scope.select_1 = "";
	//Set title to page
	document.title = 'Group Management';
	$rootScope.header_title ="Group Management";

	//Regular Expression
	$scope.titleRegex = /^\w*$/;
	$scope.pathRegex = /^\w*$/;

	//Get all group
	$scope.options = {
		type: "groups",
		qs: {
			ql: "order by path"
		}
	};
	$scope.init = function(options) {
		$('input.checkAll').prop('checked', false);
		$('#myModal').show();
		$scope.groups = [];
		var data;
		client.createCollection(options, function(err, data) {
			if (err) {
				$('#myModal').hide();
				$scope.openAlert("error", "", "Could not get group.");
			} else {
				$scope.drawGroup = function() {
					data.fetch(function(err, d) {
						$('#prevPage').hide();
						$('#nextPage').hide();

						//reset the pointer
						data.resetEntityPointer();
						$scope.groups = data._list;
						$scope.$apply($scope.groups);
						if (data._list.length <= 0) {
                            $('#messNotFound').css("display", "block");
                        }
                        else
                        	$('#messNotFound').css("display", "none");
						//if(data.hasNextPage() && data._previous.lenght!=0)
						if (data.hasNextPage())
							$('#nextPage').show();

						if (data.hasPreviousPage())
							$('#prevPage').show();
						$('#myModal').hide();
						$('#span3').height($('#span9').height());
					});
				} //End drawGroup function

				$scope.prevPage = function() {
					$('#myModal').show();
					data.getPreviousPage(function(err, data) {
						if (err)
							$scope.openAlert("error", "", "Could not get previous page.");
						else
							$scope.drawGroup();
					});
				}
				$scope.nextPage = function() {
					$('#myModal').show();
					data.getNextPage(function(err, data) {
						if (err)
							$scope.openAlert("error", "", "Could not get next page.");
						else
							$scope.drawGroup();
					});
				};
				$scope.drawGroup();
			}; //End else
		}); //End request
	}; //end init function

	//Initializing a pop-up
	$scope.opts = {
		backdropFade: true,
		dialogFade: true,
		keyboard: false,
		backdropClick: false
	};
	// "Create new group" pop-up appears
	$scope.open = function() {
		$scope.path = "";
		$scope.title = "";
		$scope.closeAlert();
		$scope.shouldBeOpen = true;
	}

	//DELETE GROUP
	$scope.deleteGroup = function() {
		var array = [];
		//get all checked checkbox
		$(".chk:checked").each(function() {
			array.push($(this).val());
		});
		if (array.length <= 0) {
			$scope.openAlert("error", "", "Please select at least a row to delete.");
			$('html, body').animate({scrollTop: '0px'}, 800);
			$('input.checkAll').prop('checked', false);
		} else {
			if (confirm($rootScope.DELETE_CONFIRM)) {
				for (var i = 0; i < array.length; i++) {
					var endpoint = 'groups/' + array[i];
					var options = {
						method: 'DELETE',
						endpoint: endpoint
					};
					client.request(options, function(err, data) {
						if (err)
							$scope.openAlert("error", "", "Could not delete group.");
						else {
							$scope.openAlert("success", "", "Deleted Successfully.");
							$scope.init($scope.options);
						}
					});
				} //End for
			} //End confirm
		}
	};

	//Create group function
	$scope.createGroup = function() {
		$('#working').show();
		//close pop-up
		$scope.shouldBeOpen = false;

		var options = {
			method: "POST",
			endpoint: "groups",
			body: {
				path: $scope.path,
				title: $scope.title
			}
		};
		client.request(options, function(err, data) {
			if (err) {
				$('#working').hide();
				$scope.openAlert("error", "", "Could not create a group.");
			} else {
				$scope.init($scope.options);
				$('#working').hide();
				$scope.openAlert("success", "", "Created Successfully.");
			}
		});
	}

	//Navigate to Group detail page
	$scope.navDetail = function(group_uuid) {
		location.href = "#/admin/groups/" + group_uuid;
	};

	//Check, Uncheck all checkbox
	$scope.check = false;
	$scope.checkAll = function() {
		if ($scope.check)
			$('input.chk').prop('checked', true);
		else
			$('input.chk').prop('checked', false);
	}
	$scope.checkItem = function() {
		var ok = false;
		$("input.chk:not(:checked)").each(function() {
			ok = true;
		});
		if (ok) {
			$('input.checkAll').prop('checked', false);
			$scope.check = false;
		} else {
			$('input.checkAll').prop('checked', true);
			$scope.check = true;
		}
	} //End check, uncheck checkbox

	//Sort function
	$scope.predicate = 'path';
	$('.sort').click(function() {
		if ($scope.reverse) {
			$('#sortImg').remove();
			$(this).append('<span id="sortImg"> &#9650;</span>');
		} else {
			$('#sortImg').remove();
			$(this).append('<span id="sortImg"> &#9660;</span>');
		}
	});

	//Control Alert
	$scope.openAlert = function(type, title, message) {
		$('#alertMsg').html('<div class="alert alert-' + type + '"><strong>' + title + '</strong> ' + message + '</div>').show('slow');
		setTimeout(function() {
			$('#alertMsg').hide('slow')
		}, 4000);
	};
	$scope.closeAlert = function() {
		$('#alertMsg').hide();
	}; //End control alert

	//SEARCH FUNCTION
	//----------------------------------------
    $scope.changeSelect = function(type) {
        if (type == "created") {
            $('#from_created').val('');
            $('#to_created').val('');
            $scope.formSearch.from_created.$error.pattern = false;
            $scope.formSearch.to_created.$error.pattern = false;
        } else {
            $('#from_modified').val('');
            $('#to_modified').val('');
            $scope.formSearch.from_modified.$error.pattern = false;
            $scope.formSearch.to_modified.$error.pattern = false;
        }
    }

    $scope.resetSearch = function() {
        $scope.titles = '';
        $scope.paths = '';
        $scope.option_1 = "";
        $scope.option_2 = "";
        $('#from_created').val('');
        $('#to_created').val('');
        $scope.formSearch.from_created.$error.pattern = false;
        $scope.formSearch.to_created.$error.pattern = false;
        $('#from_modified').val('');
        $('#to_modified').val('');
        $scope.formSearch.from_modified.$error.pattern = false;
        $scope.formSearch.to_modified.$error.pattern = false;

    }

    function convertSearchTitle(field, str) {
        var str = str.trim();
        str = str.replace(/\s+/g, " ");
        var array = str.split(" ");
        var query = '';
        if (array.length == 1) {
            query = (field + ' contains ' + "\'" + array[0] + "*\'").toString();
            query += ' OR ' + field + " = '" + str +"'";
        } else {
            for (var i = 0; i < array.length; i++) {
                query += (field + ' contains ' + "\'" + array[i] + "*\'" + ' AND ').toString();
            }
            query = query.substring(0, query.lastIndexOf('AND'));
            query += ' OR ' + field + " = '" + str +"'";
        }
        return query;
    }

    $scope.createQuery = function(titles, paths, option_1, from_created, to_created, option_2, from_modified, to_modified) {
        var query = "";

        if (titles != "") {
            query += convertSearchTitle("title", titles) + " AND ";
        }
        if (paths != "") {
            query += convertSearchTitle("path", paths) + " AND ";
        }            
        
        if (option_1 != "") {
            if (option_1 == "between") {
                if (from_created != "" && to_created != "") {
                    query += " created >= " + from_created + " AND created <= " + to_created + " AND ";
                } else {
                    //return "";
                }
            } else {
                if (from_created != "") {
                    if(option_1 != "="){
                        query += " created " + option_1 + from_created + " AND ";
                    }else{
                     var from_created2 = Date.parse($("#from_created").val() + ' 23:59:59');
                        query += " created >= " + from_created + " AND created <= " + from_created2 + " AND ";
                    }
                } else {
                   // return "";
                }
            }
        }

        if (option_2 != "") {
            if (option_2 == "between") {
                if (from_modified != "" && to_modified != "") {
                    query += " modified >= " + from_modified + " AND modified <= " + to_modified + " AND ";
                } else {
                   //return "";
                }
            } else {
                if (from_modified != "") {
                    if(option_2 != "="){
                        query += " modified " + option_2 + from_modified + " AND ";
                    }else{
                     var from_modified2 = Date.parse($("#from_modified").val() + ' 23:59:59');
                        query += " modified >= " + from_modified + " AND modified <= " + from_modified2 + " AND ";
                    }
                } else {
                    //return "";
                }
            }
        }
        query = query.substring(0, query.lastIndexOf('AND'));

        return query;
    }

    $scope.option_2 = "";
    $scope.option_1 = "";
    $scope.titles = "";
    $scope.paths = "";

    $scope.searchData = function(collection) {
       
        var from_created = '';
        var from_modified = '';
        var to_created = '';
        var to_modified = '';

        if ($scope.option_1 == "<=") {
            from_created = Date.parse($("#from_created").val() + ' 23:59:59');
        } else {
            if ($("#from_created").val() != "" && $("#from_created").val() != undefined) {
                from_created = Date.parse($("#from_created").val());
            }            
        }
        
        if ($scope.option_2 == "<=") {
            from_modified = Date.parse($("#from_modified").val() + ' 23:59:59');
        } else {
            if ($("#from_modified").val() != "" && $("#from_modified").val() != undefined) {
                from_modified = Date.parse($("#from_modified").val());
            }
        }
        
        if ($("#to_created").val() != "" && $("#to_created").val() != undefined) {
            to_created = Date.parse($("#to_created").val() + ' 23:59:59');
        }
        
        if ($("#to_modified").val() != "" && $("#to_modified").val() != undefined) {
            to_modified = Date.parse($("#to_modified").val() + ' 23:59:59');
        }

        if ($scope.titles != "" || $scope.paths != "" || $scope.option_1 != "" || $scope.option_2 != "" || from_created != "" || from_modified != "" || to_created != "" || to_modified != "") {
            $('#messNotFound').css("display", "none");
            var query = $scope.createQuery($scope.titles, $scope.paths, $scope.option_1, from_created, to_created, $scope.option_2, from_modified, to_modified);
            console.log("query =" + query);
            if(query != ""){
                 $scope.groups = {};
                var options = {
                    method: 'GET',
                    type: collection,
                    qs: {
                        ql: query
                    } //qs:{ql:"title contains 'f*'"}
                };
                $scope.init(options);
            }else{
                $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
            }
            
        } else {
            $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
        }
    }
};
//==========END GROUP CONTROLLER==========

//====================GROUP DETAIL CONTROLLER====================

function GroupDetailController($scope, $routeParams,$rootScope) {
	//Set title to page
	document.title = 'Detail Group';

	//Control 3 tab (Details, Members, Roles)
	$scope.showDetails = function() {
		$('#myTab #detailstab').tab('show');
	};
	$scope.showMembers = function() {
		$scope.initMembers();
		$('#myTab #memberstab').tab('show');
	};
	$scope.showRoles = function() {
		$scope.initRoles();
		$('#myTab #rolestab').tab('show');
	}; //End control 3 tab

	/*Control Add, Remove, Select All, Deselect All item
		Used to MEMBER:
			select_name= 'user_add_group'
			is_member = true
		Used to ROLE:
			select_name= 'role_add_group'
			is_member = false
	*/
	$scope.Add = function(select_name, is_member) {
		var arr = [];
		$('[name="' + select_name + '"] :selected').each(
			function(i, selected) {
				arr[i] = $(selected).text();

				if (is_member) {
					$scope.users_in_group.push(arr[i]);

					var index = $scope.users_add_group.indexOf(arr[i]);
					$scope.users_add_group.splice(index, 1);
				} else {
					$scope.role_of_group.push(arr[i]);

					var index = $scope.role_add_group.indexOf(arr[i]);
					$scope.role_add_group.splice(index, 1);
				}

			});
	};
	$scope.Remove = function(select_name, is_member) {
		var arr = [];
		$('[name="' + select_name + '"] :selected').each(
			function(i, selected) {
				arr[i] = $(selected).text();

				if (is_member) {
					$scope.users_add_group.push(arr[i]);

					var index = $scope.users_in_group.indexOf(arr[i]);
					$scope.users_in_group.splice(index, 1);
				} else {
					$scope.role_add_group.push(arr[i]);

					var index = $scope.role_of_group.indexOf(arr[i]);
					$scope.role_of_group.splice(index, 1);
				}

			});
	};
	$scope.SelectAll = function(select_name, is_member) {
		var arr = [];
		$('[name="' + select_name + '"] option').each(
			function(i, selected) {
				arr[i] = $(selected).val();

				if (is_member) {
					$scope.users_in_group.push(arr[i]);

					var index = $scope.users_add_group.indexOf(arr[i]);
					$scope.users_add_group.splice(index, 1);
				} else {
					$scope.role_of_group.push(arr[i]);

					var index = $scope.role_add_group.indexOf(arr[i]);
					$scope.role_add_group.splice(index, 1);
				}
			});
	};
	$scope.DeselectAll = function(select_name, is_member) {
		var arr = [];
		$('[name="' + select_name + '"] option').each(
			function(i, selected) {
				arr[i] = $(selected).val();

				if (is_member) {
					$scope.users_add_group.push(arr[i]);

					var index = $scope.users_in_group.indexOf(arr[i]);
					$scope.users_in_group.splice(index, 1);
				} else {
					$scope.role_add_group.push(arr[i]);

					var index = $scope.role_of_group.indexOf(arr[i]);
					$scope.role_of_group.splice(index, 1);
				}
			});
	}; //End control item

	//Get UUID from "list group" page by $routeParams
	$scope.uuid = $routeParams.groupid;

	//Initalizing data
	$scope.group = {};

	$scope.users_add_group = [];
	$scope.users_in_group = [];
	$scope.users_in_group_original = [];

	$scope.role_add_group = [];
	$scope.role_of_group = [];
	$scope.role_of_group_original = [];

	//Regular Expression
	$scope.pathRegex = /^\w*$/;
	$scope.titleRegex = /^([0-9a-zA-Z ])*$/;

	//Control Alert
	$scope.openAlert = function(type, title, message) {
		$('#alertMsg').html('<div class="alert alert-' + type + '"><strong>' + title + '</strong> ' + message + '</div>').show('slow');
		setTimeout(function() {
			$('#alertMsg').hide('slow')
		}, 3000);
	};
	$scope.closeAlert = function() {
		$('#alertMsg').hide();
	}; //End alert

	//---------- 1. DETAIL TAB -----------
	//Navigate to Groups page
	$scope.cancel = function() {
		$scope.closeAlert;
		location.href = "#/admin";
	};

	//Initalizing information about GROUP
	$scope.initDetails = function() {
		//Show Detail tab
		$('#myTab #detailstab').tab('show');

		//Get information about group
		var options = {
			method: "GET",
			endpoint: "groups/" + $scope.uuid
		}

		client.request(options, function(err, data) {
			if (err) {
				console.log("Could not get group");
			} else {
				$scope.group = data.entities[0];
				$scope.$apply($scope.group);
			}
		});
	}
	//Update Information About Group function
	$scope.update = function(uuid) {
		var options = {
			method: 'PUT',
			endpoint: 'groups/' + uuid,
			body: {
				path: $scope.group.path,
				title: $scope.group.title
			}
		};
		client.request(options, function(err, data) {
			if (err) {
				$scope.openAlert("success", "", "Could not update.");
			} else {
				$scope.initMembers();
				$scope.openAlert("success", "", "Updated Successfully.");
			}
		});
	};
	//---------- END DETAIL TAB -----------

	//---------- 2. USER TAB -----------
	//Initalizing information about USER
	$scope.initMembers = function() {
		$('#working').show();
		$scope.users_add_group = [];
		$scope.users_in_group = [];
		//List user in group
		var options = {
			type: "groups/" + $scope.uuid + "/users"
		};
		client.createCollection(options, function(err, data) {
			if (err) {
				$scope.openAlert("error", "Error!", "Could not get data");
			} else {
				while (data.hasNextEntity()) {
					var obj = data.getNextEntity();
					$scope.users_in_group.push(obj.get("username"));
					$scope.users_in_group_original.push(obj.get("username"));
				};
				// $scope.users_in_group_original = $scope.users_in_group;
				// console.log($scope.users_in_group_original);
				//Load list user add to group
				var options = {
					type: "users"
				};
				client.createCollection(options, function(err, data) {
					if (err) {
						console.log("Could not get users");
					} else {
						while (data.hasNextEntity()) {
							var obj = data.getNextEntity();
							if (!isExistUser(obj.get("username"))) {
								$scope.users_add_group.push(obj.get("username"));
							}
						}
						$scope.$apply($scope);
					}
				}); //end request
				$('#working').hide();
			} //end else
		}); //end get user in group
	} //End init user function

	//Add or Remove User to Group function
	$scope.updateUser = function() {
		//Remove user
		var arr_remove = [];
		//Get all User removed and push to arr_remove array.
		$scope.users_in_group_original.forEach(function(value, index, array) {
			var check = $scope.users_in_group.indexOf(value);
			if (check == -1) {
				arr_remove.push(value);
			}
		});
		if (arr_remove.length != 0) {
			arr_remove.forEach(function(value, index, array) {
				//Update item for users_in_group_original array.
				var check = $scope.users_in_group_original.indexOf(value);
				$scope.users_in_group_original.splice(check, 1);

				//Executing REMOVE User
				var options = {
					method: 'DELETE',
					endpoint: "groups/" + $scope.uuid + "/users/" + value
				};
				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not delete user.");
					} else {
						console.log("remove: " + value);
					}
				});
			});
			$scope.openAlert("success", "", "Updated Successfully.");
		}
		console.log(arr_remove);
		console.log($scope.users_in_group_original);

		//Add user
		var arr_add = [];
		//Get all User added and push to arr_add array.
		$scope.users_in_group.forEach(function(value, index, array) {
			var index = $scope.users_in_group_original.indexOf(value);
			if (index == -1) {
				arr_add.push(value);
			}
		});
		if (arr_add.length != 0) {
			arr_add.forEach(function(value, index, array) {
				//Update item for users_in_group_original array.
				$scope.users_in_group_original.push(value);

				//Executing ADD User
				var options = {
					method: "POST",
					endpoint: "groups/" + $scope.uuid + "/users/" + value
				};

				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not add user.");
					} else {
						console.log("add:" + value);
					}
				});
			});
			$scope.openAlert("success", "", "Updated Successfully.");
		}
		console.log(arr_add);
		console.log($scope.users_in_group_original);
	};

	//Check exist user

	function isExistUser(username) {
		for (var i = 0; i < $scope.users_in_group.length; i++) {
			if ($scope.users_in_group[i] == username) {
				return true;
			}
		}
		return false;
	};
	//---------- END USER TAB -----------

	//---------- 3. ROLE TAB -----------
	//Initalizing information about ROLE
	$scope.initRoles = function() {
		$('#working').show();
		$scope.role_of_group = [];
		$scope.role_add_group = [];
		//List role of group
		var options = {
			type: "groups/" + $scope.uuid + "/roles"
		};
		client.createCollection(options, function(err, data) {
			if (err) {
				console.log("Could not get role of group");
			} else {
				while (data.hasNextEntity()) {
					var obj = data.getNextEntity();
					$scope.role_of_group.push(obj.get("roleName"));
					$scope.role_of_group_original.push(obj.get("roleName"));
				};

				//Load list role add to group
				var options = {
					type: "roles"
				};
				client.createCollection(options, function(err, data) {
					if (err) {
						console.log("Could not get roles");
					} else {
						while (data.hasNextEntity()) {
							var obj = data.getNextEntity();
							if (!isExistRole(obj.get("roleName"))) {
								$scope.role_add_group.push(obj.get("roleName"));
							}
						}
						$scope.$apply($scope);
					}
				}); //end request
				$('#working').hide();
			} //end else
		}); //end get list role of group
	}; //End init role function

	//Add/Remove role to group function
	$scope.updateRole = function() {
		//Remove Role
		var arr_remove = [];
		//Get all Role removed and push to arr_remove array.
		$scope.role_of_group_original.forEach(function(value, index, array) {
			var check = $scope.role_of_group.indexOf(value);
			if (check == -1) {
				arr_remove.push(value);
			}
		});
		if (arr_remove.length != 0) {
			arr_remove.forEach(function(value, index, array) {
				//Update item for role_of_group_original array
				var check = $scope.role_of_group_original.indexOf(value);
				$scope.role_of_group_original.splice(check, 1);

				//Executing REMOVE Role
				var options = {
					method: 'DELETE',
					endpoint: "groups/" + $scope.uuid + "/roles/" + value
				};
				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not delete user.");
					} else {
						console.log('remove: ' + value);
					}
				});
			});
			$scope.openAlert("success", "", "Updated Successfully.");
		}
		
		//Add Role
		var arr_add = [];
		//Get all Role added and push to arr_add array.
		$scope.role_of_group.forEach(function(value, index, array) {
			var index = $scope.role_of_group_original.indexOf(value);
			if (index == -1) {
				arr_add.push(value);
			}
		});
		if (arr_add.length != 0) {
			arr_add.forEach(function(value, index, array) {
				//Update role_of_group_original array
				$scope.role_of_group_original.push(value);

				//Executing ADD Role
				var options = {
					method: "POST",
					endpoint: "groups/" + $scope.uuid + "/roles/" + value
				};

				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not add user.");
					} else {
						console.log('add: ' + value);
					}
				});
			});
			$scope.openAlert("success", "", "Updated Successfully.");
		}
		console.log(arr_add);
		console.log($scope.role_of_group_original);
	};

	//Check exist role

	function isExistRole(roleName) {
		for (var i = 0; i < $scope.role_of_group.length; i++) {
			if ($scope.role_of_group[i] == roleName) {
				return true;
			}
		}
		return false;
	};
	//---------- END ROLE TAB -----------
}
//==========END GROUP DETAIL CONTROLLER==========