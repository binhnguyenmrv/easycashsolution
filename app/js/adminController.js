//Admin controller

function AdminController($scope, $cookieStore) {
    $scope.user = $cookieStore.get("user");
    $scope.current_user = $cookieStore.get("current_user");
    $scope.collections = [];
    var collection = ["menus", "galleries", "users", "pages", "blogs", "categories", "groups", "stores"];
    //var collection=["menus","galleries","users","pages","blogs","categories","groups","stores","zpagings"];
    $scope.ck = false;
    $scope.templates = "";

    if ($cookieStore.get('current_user') === null || $cookieStore.get('current_user') === undefined) {
        location.href = window.location.origin + "/#/login";
        return;
    }
    $('#myModal').hide();
    $scope.setTemplate = function(name) {
        var id = "#" + name;
        $('li').removeClass("activeMenu");
        $(id).addClass("activeMenu");
        $cookieStore.put("template_name", name);
        $scope.templates = "partials/" + name + "/" + name + ".html";
    }
    $scope.redirectAdmin = function(name) {
        var id = "#" + name;
        $('li').removeClass("activeMenu");
        $(id).addClass("activeMenu");
        $cookieStore.put("template_name", name);
        $scope.templates = "partials/" + name + "/" + name + ".html";
        location.href = window.location.origin + "/#/admin";
    }

    function getListEntity(data) {
        return data.entities[0].metadata.collections;
    }

    $scope.Coll = function() {
        var options = {
            method: 'GET',
            endpoint: '/'
        };

        client.request(options, function(err, data) {
            if (err) {
                console.log(err);
                $('#myModal').hide();
                return;
            } else {
                for (index in getListEntity(data)) {
                    $.each(collection, function(key, value) {
                        if (index == value)
                            $scope.collections.push(index);
                    });
                }
                //$scope.collections = getListEntity(data);
                $scope.collections.sort();
                $scope.$apply($scope.collections);
                $('#myModal').hide();
                var template_name = $cookieStore.get("template_name");
                if (template_name != null || template_name != undefined) {
                    $scope.setTemplate(template_name);
                }

            }
        });
        // $scope.collections = collection;
        // $scope.collections.sort();
        // $scope.$apply($scope.collections);
        
    }

    $scope.logout = function() {
        $cookieStore.remove("current_user");
        $cookieStore.remove("template_name");
        $cookieStore.remove("user");
        location.href = window.location.origin + "/#/login";
        client.logout();
    }

    $scope.setting = function(user_uuid){
        location.href = "#/admin/users/" + user_uuid;
    }

    $(document).ready(function() {
        $scope.Coll();
        var template_name = $cookieStore.get("template_name");
        if (template_name != null || template_name != undefined) {
            $scope.setTemplate(template_name);
        }else{
            $scope.setTemplate("blogs");
        }
    });
}
// End Admin Controller

function MenuController($scope,$cookieStore){
    $('#myModal').hide();
}