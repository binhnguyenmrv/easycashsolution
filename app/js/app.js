'use strict';

/* App Module */

var app = angular.module('ecs', ['ecsFilters', 'ngCookies', 'ui.bootstrap', 'imageupload', 'UiEqualToModule']).
config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
		when('/login', {
			templateUrl: 'partials/login/login.html',
			controller: LoginController
		}).
		when('/register', {
			templateUrl: 'partials/login/register.html',
			controller: RegisterController
		}).
		when('/admin', {
			templateUrl: 'partials/admin/admin.html',
			controller: AdminController
		}).
		when('/admin/users/:userid', {
			templateUrl: 'partials/users/user-detail.html',
			controller: UserDetailController
		}).
		when('/admin/groups/:groupid', {
			templateUrl: 'partials/groups/group-detail.html',
			controller: GroupDetailController
		}).
		when('/users', {
			templateUrl: 'partials/users/users.html',
			controller: UserController
		}).
		otherwise({
			redirectTo: '/login'
		});
	}
]);