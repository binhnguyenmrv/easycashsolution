function RootControl($rootScope,$http) {
    $rootScope.word = /^([0-9a-zA-Z ])*$/;
    $rootScope.date = /^(((0[13578]|1[02])\/(0[1-9]|[12]\d|3[01])\/((19|[2-9]\d)\d{2}))|((0[13456789]|1[012])\/(0[1-9]|[12]\d|30)\/((19|[2-9]\d)\d{2}))|(02\/(0[1-9]|1\d|2[0-8])\/((19|[2-9]\d)\d{2}))|(02\/29\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/;
    $rootScope.openAlert = function(type, title, message) {
        $('#alertMsg').html('<div class="alert alert-' + type + '"><strong>' + title + '</strong> ' + message + '</div>').show('slow');
        setTimeout(function() {
            $('#alertMsg').hide('slow')
        }, 4000);
    };
    $rootScope.openAlert1 = function(type, title, message) {
        $('#alertMsg1').html('<div class="alert alert-' + type + '"><strong>' + title + '</strong> ' + message + '</div>').show('slow');
        setTimeout(function() {
            $('#alertMsg').hide('slow')
        }, 4000);
    };
    $rootScope.closeAlert = function() {
        $('#alertMsg').hide();
    };

    $rootScope.upload = function(image, callback) {
        var formData = new FormData();
        if(image.file != null && image.file != undefined){
             formData.append('image', image.file, image.file.name);
            $http.post('/upload', formData, {
                headers: { 'Content-Type': false },
                transformRequest: angular.identity
            }).success(function(result) {
                $rootScope.uploadedImgSrc = result.src;
                $rootScope.sizeInBytes = result.size;
                callback();
            });
        }else{
            $rootScope.uploadedImgSrc = image.src;
            callback();
        }
       
    };

    $rootScope.deleteImage = function(path, callback) {
        var formData = new FormData();
        console.log(path);
        
        formData.append('path', path);
        $http.post('/delete', formData, {
            headers: { 'Content-Type': false },
            transformRequest: angular.identity
        }).success(function(result) {
            $rootScope.success = true;
            callback();
        });
    };

    
    $rootScope.CREATE_SUCCESS = "Created Successfully.";
    $rootScope.DELETE_SUCCESS = "Deleted Successfully.";
    $rootScope.UPDATE_SUCCESS = "Updated Successfully.";
    $rootScope.DELETE_SELECT = "Please select at least a row to delete.";
    $rootScope.DELETE_CONFIRM = "Are you sure you want to delete selected records?"
    $rootScope.header_title = "";
    $rootScope.REQUIRED = " is a mandatory field.";
    $rootScope.PATTERN = " must contain only characters a-z, A-Z, 0-9.";
}