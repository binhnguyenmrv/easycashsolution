//categoriesController

function categoriesController($scope, $rootScope) {
	document.title = 'Category Management';
	$scope.title = "";
	$scope.name = "";
	$scope.search = '';
	$scope.select_1 = "";
	$scope.select_2 = "";
	$rootScope.header_title ="Category Management";
	//  	$scope.filterByTitle = function(data){
	// 	return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
	// }
	//Get categories from server
	$scope.options = {
		type: "categories",
	}
	// init data
	$scope.init = function(options) {
		$('input.checkAll').prop('checked', false);
		$('#myModal').show();
		$scope.categories = [];
		var data;
		client.createCollection(options, function(err, data) {
			if (err) {
				$('#myModal').hide();
				location.reload();
			} else {
				console.log(data);
				$scope.drawPages2 = function() {
					data.fetch(function(err, d) {
						$('#prevPage').hide();
						$('#nextPage').hide();
						//reset the pointer
						data.resetEntityPointer();
						$scope.categories = data._list;
						$scope.categories.sort();
						$scope.$apply($scope.categories);
						if (data._list.length <= 0) {
                            $('#messNotFound').css("display", "block");
                        }
                        else
                        	$('#messNotFound').css("display", "none");
						if (data.hasNextPage())
							$('#nextPage').show();
						if (data.hasPreviousPage())
							$('#prevPage').show();
						$('#myModal').hide();
						$('#span3').height($('#span9').height());
					});
				} //End drawUser function

				$scope.prevPage = function() {
					data.getPreviousPage(function(err, data) {
						if (err)
							alert("err");
						else
							$scope.drawPages2();
					});
				}
				$scope.nextPage = function() {
					data.getNextPage(function(err, data) {
						if (err)
							alert("err");
						else
							$scope.drawPages2();
					});
				};
				$scope.drawPages2();
			}; //End else
		});
	};

	$scope.opts = {
		backdropFade: true,
		dialogFade: true
	};

	//"Create new category" pop-up appears
	$scope.open = function() {
		$scope.shouldBeOpen = true;
		$scope.title = "";
	};
	var old_name = {};
	$scope.openDetail = function(data) {
		old_name = data.title;
		$scope.shouldBeOpenDetail = true;
		$scope.title = data.title;
		$scope.name = data.name;
	}
	$scope.changeForm = function(obj) {
		console.log(obj);
		console.log(old_name);
		var b = angular.equals(old_name, obj);
		if (b) {
			$('#btn_update_store').attr("disabled", "disabled");
		} else {
			$('#btn_update_store').removeAttr("disabled", "disabled");
		}
	};
	//"Create new category" pop-up disappears
	$scope.close = function() {
		$scope.shouldBeOpen = false;
		$scope.shouldBeOpenDetail = false;
	}
	//Delete category function
	$scope.array = [];
	$scope.delete = function() {
		$(".chk:checked").each(function() {
			$scope.array.push($(this).val());
		});
		if ($scope.array.length == 0) {
			$scope.openAlert("error", "", "Please select at least a row to delete.");
			$('html, body').animate({scrollTop: '0px'}, 800);
		} else
		if (confirm($rootScope.DELETE_CONFIRM)) {
			//get all checked checkbox
			$('#myModal').show();
			for (var i = 0; i < $scope.array.length; i++) {
				var endpoint = 'categories/' + $scope.array[i];
				var options = {
					method: 'DELETE',
					endpoint: endpoint
				};
				client.request(options, function(err, data) {
					if (err)
						$scope.openAlert("error", "", "Could not delete categories.");
					else {
						$('#myModal').hide();
						$scope.init($scope.options);
						$scope.openAlert("success", "", "Deleted Successfully.");
						$('input.checkAll').prop('checked', false);
						$scope.check = true;
					}
				});
			}

		}
	};

	//Create new category function
	$scope.createCategory = function() {
		$('#working').show();
		$scope.name = $scope.title.replace(/\s/g, "");
		var options = {
			method: "POST",
			endpoint: "categories",
			body: {
				title: $scope.title,
				name: $scope.name,
			}
		};
		$scope.shouldBeOpen = false;
		client.request(options, function(err, data) {
			if (err) {
				$('#working').hide();
				$scope.openAlert("error", "", "Catagory exist.");
			} else {
				$('#working').hide();
				$scope.init($scope.options);
				$scope.openAlert("success", "", "Created Successfully.");
				//location.reload();
			}
		});
	};
	// Edit category function
	$scope.updateCategory = function() {
		$('#working1').show();
		var options = {
			method: "PUT",
			endpoint: "categories/" + $scope.name,
			body: {
				title: $scope.title
			}
		};
		$scope.shouldBeOpenDetail = false;
		client.request(options, function(err, data) {
			if (err) {
				$('#working1').hide();
				$scope.openAlert("error", "", "Catagory exist.");
			} else {
				$('#working1').hide();
				$scope.init($scope.options);
				$scope.openAlert("success", "", "Updated Successfully.");
			}
		});
	};
	//sort
	$scope.predicate = '_data.title';
	$('.sort').click(function() {
		if ($scope.reverse) {
			$('#sortImg').remove();
			$(this).append('<span id="sortImg"> &#9650;</span>');
		} else {
			$('#sortImg').remove();
			$(this).append('<span id="sortImg"> &#9660;</span>');
		}
	});

	$scope.check = true;
	$scope.checkAll = function(data) {
		console.log(data)
		if (data)
			$('input.chk').prop('checked', true);
		else
			$('input.chk').prop('checked', false);
	}
	$scope.check1 = function() {
		var d = false;
		$("input.chk:not(:checked)").each(function() {
			d = true;
		});
		if (d) {
			$('input.checkAll').prop('checked', false);
			$scope.check = true;
		} else {
			$('input.checkAll').prop('checked', true);
			$scope.check = false;
		}
	}
	//Search
	//----------SEARCH----------------
    $scope.changeSelect = function(type) {
        if (type == "created") {
            $('#from_created').val('');
            $('#to_created').val('');
            $scope.formSearch.from_created.$error.pattern = false;
            $scope.formSearch.to_created.$error.pattern = false;
        } else {
            $('#from_modified').val('');
            $('#to_modified').val('');
            $scope.formSearch.from_modified.$error.pattern = false;
            $scope.formSearch.to_modified.$error.pattern = false;
        }
    }

    $scope.resetSearch = function() {
        $scope.names = '';
        $scope.option_1 = "";
        $scope.option_2 = "";
        $('#from_created').val('');
        $('#to_created').val('');
        $scope.formSearch.from_created.$error.pattern = false;
        $scope.formSearch.to_created.$error.pattern = false;
        $('#from_modified').val('');
        $('#to_modified').val('');
        $scope.formSearch.from_modified.$error.pattern = false;
        $scope.formSearch.to_modified.$error.pattern = false;

    }

    function convertSearchTitle(field, str) {
        var str = str.trim();
        str = str.replace(/\s+/g, " ");
        var array = str.split(" ");
        var query = '';
        if (array.length == 1) {
            query = (field + ' contains ' + "\'" + array[0] + "*\'").toString();
            query += ' OR ' + field + " = '" + str +"'";
        } else {
            for (var i = 0; i < array.length; i++) {
                query += (field + ' contains ' + "\'" + array[i] + "*\'" + ' AND ').toString();
            }
            query = query.substring(0, query.lastIndexOf('AND'));
            query += ' OR ' + field + " = '" + str +"'";
        }
        return query;
    }

    $scope.createQuery = function(titles, paths, option_1, from_created, to_created, option_2, from_modified, to_modified) {
        var query = "";

        if (titles != "") {
            query += convertSearchTitle("title", titles) + " AND ";
        }
        if (paths != "") {
            query += convertSearchTitle("path", paths) + " AND ";
        }            
        
        if (option_1 != "") {
            if (option_1 == "between") {
                if (from_created != "" && to_created != "") {
                    query += " created >= " + from_created + " AND created <= " + to_created + " AND ";
                } else {
                    //return "";
                }
            } else {
                if (from_created != "") {
                    if(option_1 != "="){
                        query += " created " + option_1 + from_created + " AND ";
                    }else{
                     var from_created2 = Date.parse($("#from_created").val() + ' 23:59:59');
                        query += " created >= " + from_created + " AND created <= " + from_created2 + " AND ";
                    }
                } else {
                   // return "";
                }
            }
        }

        if (option_2 != "") {
            if (option_2 == "between") {
                if (from_modified != "" && to_modified != "") {
                    query += " modified >= " + from_modified + " AND modified <= " + to_modified + " AND ";
                } else {
                   //return "";
                }
            } else {
                if (from_modified != "") {
                    if(option_2 != "="){
                        query += " modified " + option_2 + from_modified + " AND ";
                    }else{
                     var from_modified2 = Date.parse($("#from_modified").val() + ' 23:59:59');
                        query += " modified >= " + from_modified + " AND modified <= " + from_modified2 + " AND ";
                    }
                } else {
                    //return "";
                }
            }
        }
        query = query.substring(0, query.lastIndexOf('AND'));

        return query;
    }

    $scope.option_2 = "";
    $scope.option_1 = "";
    $scope.names = "";

    $scope.searchData = function(collection) {
        
        var from_created = '';
        var from_modified = '';
        var to_created = '';
        var to_modified = '';

        if ($scope.option_1 == "<=") {
            from_created = Date.parse($("#from_created").val() + ' 23:59:59');
        } else {
            if ($("#from_created").val() != "" && $("#from_created").val() != undefined) {
                from_created = Date.parse($("#from_created").val());
            }            
        }
        
        if ($scope.option_2 == "<=") {
            from_modified = Date.parse($("#from_modified").val() + ' 23:59:59');
        } else {
            if ($("#from_modified").val() != "" && $("#from_modified").val() != undefined) {
                from_modified = Date.parse($("#from_modified").val());
            }
        }
        
        if ($("#to_created").val() != "" && $("#to_created").val() != undefined) {
            to_created = Date.parse($("#to_created").val() + ' 23:59:59');
        }
        
        if ($("#to_modified").val() != "" && $("#to_modified").val() != undefined) {
            to_modified = Date.parse($("#to_modified").val() + ' 23:59:59');
        }

        if ($scope.names != "" || $scope.option_1 != "" || $scope.option_2 != "" || from_created != "" || from_modified != "" || to_created != "" || to_modified != "") {
            $('#messNotFound').css("display", "none");
            var query = $scope.createQuery($scope.names, $scope.option_1, from_created, to_created, $scope.option_2, from_modified, to_modified);
            console.log("query =" + query);
            if(query != ""){
                $('#workingSearch').show();
                $scope.categories = {};
                var options = {
                    method: 'GET',
                    type: collection,
                    qs: {
                        ql: query
                    } //qs:{ql:"title contains 'f*'"}
                };
				$scope.init(options);
            }else{
                $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
            }
            
        } else {
            $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
        }
    }
	

};