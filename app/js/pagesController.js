'use strict';

var page_current = null;
var menu_current = null;

function PageController($scope, $cookieStore, $rootScope) {
    $scope.pages = [];
    $scope.title = "";
    $scope.name = "";
    $scope.description = "";
    $scope.created = "";
    $scope.modified = "";
    $scope.status = "0";
    $scope.picture = "";
    document.title = 'Page Management';
    $scope.page = {};
    $scope.status_selected = "";
    $rootScope.header_title ="Page Management";

    //Get all user
    $scope.options = {
        type: "pages",
        qs: {
            limit: 10
        }
    };
    $scope.init = function(options) {
        $('input.checkAll').prop('checked', false);
        $('#messNotFound').css("display", "none");
        $('#myModal').show();
        $scope.select_1 = "";
        $scope.pages = [];
        var data;
        client.createCollection(options, function(err, data) {
            $('#table').css("display", "");
            if (err) {
                $scope.openAlert("error", "", data.error_description);
                $('#myModal').hide();
            } else {
                if (data._list.length <= 0) {
                    $('#messNotFound').css("display", "block");
                }
                $scope.drawPages2 = function() {
                    data.fetch(function(err, d) {
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.pages = data._list;
                        $scope.$apply($scope.pages);

                        if (data.hasNextPage())
                            $('#nextPage').show();
                        if (data.hasPreviousPage())
                            $('#prevPage').show();

                        $('#myModal').hide();
                        $scope.checkAll(false);
                        $('#span3').height($('#span9').height());
                    });
                } //End drawUser function

                $scope.prevPage = function() {
                    data.getPreviousPage(function(err, data) {
                        if (err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function() {
                    data.getNextPage(function(err, data) {
                        if (err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            }; //End else
        });
    };

    $scope.search = '';

    $scope.check = true;
    $scope.checkAll = function(data) {
        if (data)
            $('input.chk').prop('checked', true);
        else
            $('input.chk').prop('checked', false);
    }

    $scope.check1 = function() {
        var d = false;
        $("input.chk:not(:checked)").each(function() {
            d = true;
        });
        if (d) {
            $('input.checkAll').prop('checked', false);
            $scope.check = true;
        } else {
            $('input.checkAll').prop('checked', true);
            $scope.check = false;
        }
    }

    // When the editor loses the focus, tell the change event to fire so Lightswitch recognises that the value needs saving...
    $(document).on("keyup", "#new_textarea .redactor_editor", function() {
        $("#redactor1").trigger("change");
    });

    $(document).on("change", "#redactor1", function(data) {
        console.log($('#redactor1').val());
    });
    $(document).on("keyup", "#edit_textarea .redactor_editor", function() {
        console.log($('#redactor2').val());
        $("#redactor2").trigger("change");
    });

    $(document).on("change", "#redactor2", function(data) {
        $scope.changeForm($scope.page);
    });

    $scope.changeForm = function(obj) {
        obj.description = $('#redactor2').val();
        var b = angular.equals(page_current, obj);
        if (b) {
            $('#btn_update_page').attr("disabled", "disabled");
        } else {
            if(!$scope.formUpdate.title.$invalid){
                $('#btn_update_page').removeAttr("disabled", "disabled");
            }
        }
    };

    $scope.open = function() {
        $scope.page = {};
        $scope.page.description = "";
        $scope.page.status = "0";
        $('#redactor1').val("");
        $scope.formAdd.title.$pristine = true;
        $scope.formAdd.title.$invalid = false;
        $scope.formAdd.title.$dirty = false;
    };

    $scope.cancel = function() {
        location.href = "#/admin";
    };

    //Delete pages function
    $scope.deletePage = function() {
        var array = [];
        $(".chk:checked").each(function() {
            array.push($(this).val());
        });
        if (array.length > 0) {
            if (confirm($rootScope.DELETE_CONFIRM)) {
                $('#myModal').show();
                for (var i = 0; i < array.length; i++) {
                    var endpoint = 'pages/' + array[i];
                    var options = {
                        method: 'DELETE',
                        endpoint: endpoint
                    };
                    client.request(options, function(err, data) {
                        $('#myModal').hide();
                        if (err)
                            $scope.openAlert("error", "", data.error_description);
                        else {
                            $scope.openAlert("success", "", $scope.DELETE_SUCCESS);
                            $scope.init($scope.options);
                        }
                    });
                }
            }
        } else {
            $scope.openAlert("error", "", $scope.DELETE_SELECT);
            $('html, body').animate({scrollTop: '0px'}, 800);
        }
    };


    $scope.createPage = function(page) {
        page.description = $('#redactor1').val();
        $('#working').show();
        if (page.status != "1") {
            page.status = "0";
        }
        var options = {
            method: "POST",
            endpoint: "pages",
            body: {
                title: page.title,
                name: page.title,
                description: page.description,
                status: page.status,
                picture: ""
            }
        };

        client.request(options, function(err, data) {
            $('#working').hide();
            if (err) {
                $scope.openAlert1("error", "", data.error_description);
            } else {
                $scope.openAlert("success", "", $scope.CREATE_SUCCESS);
                $('#addBlog').hide();
                $('.modal-backdrop').remove();
                $scope.init($scope.options);
            }
        });

    };


    //Update Page function
    $scope.updatePage = function(page) {
        console.log(page);
        page.description = $('#redactor2').val();
        $('#working1').show();
        if (page.title !== undefined) {
            if (page.status != "1") {
                page.status = "0";
            }
            var options = {
                method: 'PUT',
                endpoint: 'pages/' + page.uuid,
                body: {
                    title: page.title,
                    description: page.description,
                    status: page.status,
                    picture: ""
                }
            };
            client.request(options, function(err, data) {
                $('#working1').hide();
                if (err) {
                    $scope.openAlert1("error", "", data.error_description);
                } else {
                    $('#editBlog').hide();
                    $('.modal-backdrop').remove();
                    $scope.openAlert("success", "", $scope.UPDATE_SUCCESS);
                    $scope.init($scope.options);
                }
            });
        } else {
            $scope.openAlert("error", "", "Input data invalid, title field is required!");
        }
    }

    $scope.editModal = function(data) {
        $('#redactor2').redactor({
            minHeight: 200
        });
        $("#editBlog").css({
            "top": "-50%"
        }).show().animate({
            "top": "50%"
        }, 300);
        $(".redactor_editor").html(data);
        $("body").append('<div class="modal-backdrop fade in"></div>');
        return false;
    }


    $scope.pageDetail = function(obj) {
        console.log(obj);
        page_current = obj;
        $scope.page = JSON.parse(JSON.stringify(obj));
        $scope.editModal($scope.page.description);
        $('#btn_update_page').attr("disabled", "disabled");
    };

    $scope.resetPage = function() {
        $scope.title = "";
        $scope.name = "";
        $scope.description = "";
        $scope.status = 0;
        $scope.picture = "";
    };

    $scope.predicate = '_data.name';
    $('.sort').click(function() {
        if ($scope.reverse) {
            $('#sortImg1').remove();
            $(this).append('<span id="sortImg1"> &#9650;</span>');
        } else {
            $('#sortImg1').remove();
            $(this).append('<span id="sortImg1"> &#9660;</span>');
        }
    });

    //----------SEARCH----------------
    $scope.changeSelect = function(type) {
        if (type == "created") {
            $('#from_created').val('');
            $('#to_created').val('');
            $scope.formSearch.from_created.$error.pattern = false;
            $scope.formSearch.to_created.$error.pattern = false;
        } else {
            $('#from_modified').val('');
            $('#to_modified').val('');
            $scope.formSearch.from_modified.$error.pattern = false;
            $scope.formSearch.to_modified.$error.pattern = false;
        }
    }

    $scope.resetSearch = function() {
        $scope.titles = '';
        $scope.status_selected = '';
        $scope.option_1 = "";
        $scope.option_2 = "";
        $('#from_created').val('');
        $('#to_created').val('');
        $scope.formSearch.from_created.$error.pattern = false;
        $scope.formSearch.to_created.$error.pattern = false;
        $('#from_modified').val('');
        $('#to_modified').val('');
        $scope.formSearch.from_modified.$error.pattern = false;
        $scope.formSearch.to_modified.$error.pattern = false;

    }

    function convertSearchTitle(field, str) {
        var str = str.trim();
        str = str.replace(/\s+/g, " ");
        var array = str.split(" ");
        var query = '';
        if (array.length == 1) {
            query = (field + ' contains ' + "\'" + array[0] + "*\'").toString();
            query += ' OR ' + field + " = '" + str +"'";
        } else {
            for (var i = 0; i < array.length; i++) {
                query += (field + ' contains ' + "\'" + array[i] + "*\'" + ' AND ').toString();
            }
            query = query.substring(0, query.lastIndexOf('AND'));
            query += ' OR ' + field + " = '" + str +"'";
        }
        return query;
    }

    $scope.createQuery = function(titles, status_selected, option_1, from_created, to_created, option_2, from_modified, to_modified) {
        var query = "";

        if (titles != "") {
            query += convertSearchTitle("title", titles) + " AND ";
        }
        if (status_selected != "") {
            if (status_selected == "1") {
                query += " status = \'1\' AND ";
            } else {
                query += " status = \'0\' AND ";
            }
        }            
        
        if (option_1 != "") {
            if (option_1 == "between") {
                if (from_created != "" && to_created != "") {
                    query += " created >= " + from_created + " AND created <= " + to_created + " AND ";
                } else {
                    //return "";
                }
            } else {
                if (from_created != "") {
                     if(option_1 != "="){
                        query += " created " + option_1 + from_created + " AND ";
                    }else{
                     var from_created2 = Date.parse($("#from_created").val() + ' 23:59:59');
                        query += " created >= " + from_created + " AND created <= " + from_created2 + " AND ";
                    }
                } else {
                   // return "";
                }
            }
        }

        if (option_2 != "") {
            if (option_2 == "between") {
                if (from_modified != "" && to_modified != "") {
                    query += " modified >= " + from_modified + " AND modified <= " + to_modified + " AND ";
                } else {
                   //return "";
                }
            } else {
                if (from_modified != "") {
                    if(option_2 != "="){
                        query += " modified " + option_2 + from_modified + " AND ";
                    }else{
                     var from_modified2 = Date.parse($("#from_modified").val() + ' 23:59:59');
                        query += " modified >= " + from_modified + " AND modified <= " + from_modified2 + " AND ";
                    }
                } else {
                    //return "";
                }
            }
        }
        query = query.substring(0, query.lastIndexOf('AND'));

        return query;
    }

    $scope.option_2 = "";
    $scope.option_1 = "";
    $scope.titles = "";

    $scope.searchData = function(collection) {
        
        var from_created = '';
        var from_modified = '';
        var to_created = '';
        var to_modified = '';

        if ($scope.option_1 == "<=") {
            from_created = Date.parse($("#from_created").val() + ' 23:59:59');
        } else {
            if ($("#from_created").val() != "" && $("#from_created").val() != undefined) {
                from_created = Date.parse($("#from_created").val());
            }            
        }
        
        if ($scope.option_2 == "<=") {
            from_modified = Date.parse($("#from_modified").val() + ' 23:59:59');
        } else {
            if ($("#from_modified").val() != "" && $("#from_modified").val() != undefined) {
                from_modified = Date.parse($("#from_modified").val());
            }
        }
        
        if ($("#to_created").val() != "" && $("#to_created").val() != undefined) {
            to_created = Date.parse($("#to_created").val() + ' 23:59:59');
        }
        
        if ($("#to_modified").val() != "" && $("#to_modified").val() != undefined) {
            to_modified = Date.parse($("#to_modified").val() + ' 23:59:59');
        }

        if ($scope.titles != "" || $scope.status_selected != "" || $scope.option_1 != "" || $scope.option_2 != "" || from_created != "" || from_modified != "" || to_created != "" || to_modified != "") {
            $('#messNotFound').css("display", "none");
            var query = $scope.createQuery($scope.titles, $scope.status_selected, $scope.option_1, from_created, to_created, $scope.option_2, from_modified, to_modified);
            console.log("query =" + query);
            if(query != ""){
                //$('#workingSearch').show();
                $scope.pages = {};
                var options = {
                    method: 'GET',
                    type: collection,
                    qs: {
                        ql: query
                    } 
                };
                 $scope.init(options);
                // client.createCollection(options, function(err, data) {
                //     if (err) {
                //         $('#workingSearch').hide();
                //         $scope.openAlert("error", "", data.error_description);
                //     } else {
                //         $scope.pages = data._list;
                //         $scope.$apply($scope.pages);
                //         $('#workingSearch').hide();
                //         if (data._list.length <= 0) {
                //             $('#messNotFound').css("display", "block");
                //         }
                //     }
                // });
            }else{
                $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
            }
            
        } else {
            $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
        }
    }
}


// ================== Menu CONTROLLER ===================

function MenusController($scope, $rootScope) {

    $scope.menus = [];
    $scope.title = "";
    $scope.name = "";
    $scope.order = 0;
    $scope.created = "";
    $scope.modified = "";
    $scope.parrent = "";
    $scope.path = "";
    document.title = 'Menu Management';
    $scope.menu = {};
    $scope.select_2 = "";
    $scope.select_1 = "";
    $rootScope.header_title ="Menu Management";

    $scope.options = {
        type: "menus",
        qs: {
            limit: 10
        }
    };
    // $('#accordion2').collapse();
    $scope.init = function(options) {
        $('input.checkAll').prop('checked', false);
        $('#messNotFound').css("display", "none");
        $('#myModal').show();
        $scope.select_1 = "";
        $scope.menus = [];
        var data;
        client.createCollection(options, function(err, data) {
            $('#table').css("display", "");
            if (err) {
                $scope.openAlert("error", "", data.error_description);
            } else {
                if (data._list.length <= 0) {
                    $('#messNotFound').css("display", "block");
                }
                $scope.drawPages2 = function() {
                    data.fetch(function(err, d) {
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.menus = data._list;
                        $scope.$apply($scope.menus);

                        if (data.hasNextPage())
                            $('#nextPage').show();
                        if (data.hasPreviousPage())
                            $('#prevPage').show();
                        $scope.checkAll(false);
                        $('#span3').height($('#span9').height());
                    });
                } //End drawUser function

                $scope.prevPage = function() {
                    data.getPreviousPage(function(err, data) {
                        if (err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function() {
                    data.getNextPage(function(err, data) {
                        if (err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            } //End else
            $('#myModal').hide();
        });
    };

    $scope.search = '';
  
    $scope.check = true;
    $scope.checkAll = function(data) {
        if (data)
            $('input.chk').prop('checked', true);
        else
            $('input.chk').prop('checked', false);
    }
    $scope.check1 = function() {
        var d = false;
        $("input.chk:not(:checked)").each(function() {
            d = true;
        });
        if (d) {
            $('input.checkAll').prop('checked', false);
            $scope.check = true;
        } else {
            $('input.checkAll').prop('checked', true);
            $scope.check = false;
        }
    }

    //"Create new user" pop-up appears
    $scope.open = function() {
        //$scope.modalCreateMenu = true;
        //$scope.initCreate();
        ///location.href="#/admin/createmenu";
    };

    $scope.cancel = function() {
        location.href = "#/admin";
    }

    //Delete pages function
    $scope.deleteMenus = function() {
        var array = [];
        $(".chk:checked").each(function() {
            array.push($(this).val());
        });
        if (array.length > 0) {
            if (confirm($rootScope.DELETE_CONFIRM)) {
                $('#myModal').show();
                for (var i = 0; i < array.length; i++) {
                    var endpoint = 'menus/' + array[i];
                    var options = {
                        method: 'DELETE',
                        endpoint: endpoint
                    };
                    client.request(options, function(err, data) {
                        $('#myModal').hide();
                        if (err)
                            $scope.openAlert("error", "", data.error_description);
                        else {
                            $scope.openAlert("success", "", $scope.DELETE_SUCCESS);
                            $scope.init($scope.options);
                        }
                    });
                }
            }
        } else {
            $scope.openAlert("error", "", $scope.DELETE_SELECT);
            $('html, body').animate({scrollTop: '0px'}, 800);
        }
    }

    $scope.changeForm = function(obj) {

        var b = angular.equals(menu_current, obj);
        if (b) {
            $('#btn_update_menu').attr("disabled", "disabled");
        } else {
            if(!$scope.formUpdate.title.$invalid){
                $('#btn_update_menu').removeAttr("disabled", "disabled");
            }
        }
    };

    $scope.menuDetail = function(obj) {
        menu_current = obj;
        $scope.menu = JSON.parse(JSON.stringify(obj));
        $('#btn_update_menu').attr("disabled", "disabled");
    }

    $scope.predicate = '_data.name';
    $('.sort').click(function() {
        if ($scope.reverse) {
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9650;</span>');
        } else {
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9660;</span>');
        }
    });

    $(document).ready(function() {
        $scope.init($scope.options);
    });


    $scope.menu = {};
    $scope.categories = [];
    $scope.pages = [];
    $scope.titleCustom = "";
    $scope.urlCustom = "";

    $scope.checkPage = false;
    $scope.checkCate = false;

    $scope.getCategories = function() {
        $('#workingAdd').show();
        var options = {
            type: "categories"
        }

        client.createCollection(options, function(err, data) {
            if (err) {
                $scope.openAlert1("error", "", data.error_description);
                return;
            } else {
                $scope.categories = data._list;
                $scope.$apply($scope.categories);
            }
            $('#workingAdd').hide();
        });
    }

    $scope.getAllPages = function() {
        $('#workingAdd').show();
        var options = {
            type: "pages"
        }

        client.createCollection(options, function(err, data) {
            if (err) {
                $scope.openAlert1("error", "", data.error_description);
                return;
            } else {
                $scope.pages = data._list;
                $scope.$apply($scope.pages);
            }
            $('#workingAdd').hide();
        });
    }

    $scope.addLinkToMenu = function() {
        var newMenu = {};
        newMenu.title = $scope.titleCustom;
        newMenu.name = $scope.titleCustom;

        if ($scope.urlCustom.indexOf('#/') < 0 || $scope.urlCustom.indexOf('#/') > 0) {
            newMenu.path = "#/" + $scope.urlCustom;
        } else {
            newMenu.path = $scope.urlCustom;
        }

        newMenu.order = 1;
        newMenu.parrent = 1;
        $scope.createMenu(newMenu);
    }

    $scope.addPageToMenu = function(pages) {
        for (var p in pages) {
            if (pages[p]._data.ischecked == true) {
                var newMenu = {};
                newMenu.title = pages[p]._data.title;
                newMenu.name = pages[p]._data.title;
                newMenu.path = "#/" + pages[p]._data.title;
                newMenu.order = 1;
                newMenu.parrent = 1;
                $scope.createMenu(newMenu);
            }
        }
    }

    $scope.addCategoryToMenu = function(categories) {
        for (var p in categories) {
            if (categories[p]._data.ischecked == true) {
                var newMenu = {};
                newMenu.title = categories[p]._data.title;
                newMenu.name = categories[p]._data.title;
                newMenu.path = "#/" + categories[p]._data.title;
                newMenu.order = 1;
                newMenu.parrent = 1;
                $scope.createMenu(newMenu);
            }
        }
    }

    $scope.initCreate = function() {
        $scope.getCategories();
        $scope.getAllPages();
    }

    $scope.getAllMenus = function() {
        $('#workingAdd').show();
        var options = {
            type: "menus"
        }

        client.createCollection(options, function(err, data) {
            if (err) {
                $scope.openAlert1("error", "", data.error_description);
                return;
            } else {
                $scope.menus = data._list;
                $scope.$apply($scope.menus);
                $('#workingAdd').hide();
            }
        });
    }

    $scope.createMenu = function(newMenu) {
        $('#workingAdd').show();
        var options = {
            method: "POST",
            endpoint: "menus",
            body: {
                title: newMenu.title,
                name: newMenu.name,
                path: newMenu.path,
                order: newMenu.order,
                parrent: newMenu.parrent
            }
        };

        client.request(options, function(err, data) {
            $('#workingAdd').hide();
            if (err) {
                $scope.openAlert1("error", "", data.error_description);
            } else {
                $scope.openAlert1("success", "", $scope.CREATE_SUCCESS);
                showMessCom('#messagebox', '<div class="alert alert-success"> Created successfuly.</div>');
                $scope.resetForm();
                $scope.getAllMenus();
            }
        });
    }

    $scope.updateMenu = function(menu) {
        $('#workingAdd').show();
        console.log(menu.path.indexOf('#/'));
        if (menu.path.indexOf('#/') < 0 || menu.path.indexOf('#/') > 0) {
            menu.path = "#/" + menu.path;
        }
        var options = {
            method: "PUT",
            endpoint: "menus/" + menu.uuid,
            body: {
                title: menu.title,
                path: menu.path,
                order: menu.order,
                parrent: menu.parrent
            }
        };

        client.request(options, function(err, data) {
            $('#workingAdd').hide();
            if (err) {
                $scope.openAlert1("error", "", data.error_description);
            } else {
                $scope.openAlert1("success", "", $scope.UPDATE_SUCCESS);
                $scope.getAllMenus();
            }
        });
    }

    $scope.updateDetailMenu = function(menu) {
        $('#working').show();
        if ($scope.menu.path.indexOf('#/') < 0 || $scope.menu.path.indexOf('#/') > 0) {
            $scope.menu.path = "#/" + $scope.menu.path;
        }
        var options = {
            method: 'PUT',
            endpoint: 'menus/' + menu.uuid,
            body: {
                title: $scope.menu.title,
                path: $scope.menu.path,
                order: $scope.menu.order,
                parrent: $scope.menu.parrent
            }
        };
        client.request(options, function(err, data) {
            $('#working').hide();
            if (err)
                $scope.openAlert1("error", "", data.error_description);
            // showMessCom('#messMenuDetail', '<div class="alert alert-error">' + data.error_description + '</div>');
            else {
                $('#myModalUpdate').modal('hide');
                $scope.openAlert("success", "", $scope.UPDATE_SUCCESS);
                $scope.init($scope.options);
            }
        });
    }


    $scope.deleteMenu = function(menu) {
        $('#workingAdd').show();
        var options = {
            method: "DELETE",
            endpoint: "menus/" + menu.uuid
        };

        client.request(options, function(err, data) {
            $('#workingAdd').hide();
            if (err)
                $scope.openAlert1("error", "", data.error_description);
            else {
                $scope.openAlert1("success", "", $scope.DELETE_SUCCESS);
                $scope.getAllMenus();
            }
        });
    }

    $scope.selectAllCategories = function(checked) {
        for (var p in $scope.categories) {
            $scope.categories[p]._data.ischecked = checked;
        }
    }

    $scope.selectAllPages = function(checked) {
        for (var p in $scope.pages) {
            $scope.pages[p]._data.ischecked = checked;
        }
    }

    $scope.resetForm = function() {
        $scope.titleCustom = "";
        $scope.urlCustom = "";

        $scope.selectAllPages(false);
        $scope.selectAllCategories(false);
    }

    $scope.change_check_pages = function() {
        var i = 0;
        for (var p in $scope.pages) {
            if ($scope.pages[p]._data.ischecked == false) {
                $scope.checkPage = false;
                return;
            } else {
                i = i + 1;
            }
        }
        if (i == $scope.pages.length) {
            $scope.checkPage = true;
        } else {
            $scope.checkPage = false;
        }
    }

    $scope.change_check_categories = function() {
        var i = 0;
        for (var p in $scope.categories) {
            if ($scope.categories[p]._data.ischecked == true) {
                i = i + 1;
            } else {
                $scope.checkCate = false;
                return;
            }
        }
        if (i == $scope.categories.length) {
            $scope.checkCate = true;
        } else {
            $scope.checkCate = false;
        }
    }
    //----------------------------------------
    $scope.changeSelect = function(type) {
        if (type == "created") {
            $('#from_created').val('');
            $('#to_created').val('');
            $scope.formSearch.from_created.$error.pattern = false;
            $scope.formSearch.to_created.$error.pattern = false;
        } else {
            $('#from_modified').val('');
            $('#to_modified').val('');
            $scope.formSearch.from_modified.$error.pattern = false;
            $scope.formSearch.to_modified.$error.pattern = false;
        }
    }

    $scope.resetSearch = function() {
        $scope.titles = '';
        $scope.paths = '';
        $scope.option_1 = "";
        $scope.option_2 = "";
        $('#from_created').val('');
        $('#to_created').val('');
        $scope.formSearch.from_created.$error.pattern = false;
        $scope.formSearch.to_created.$error.pattern = false;
        $('#from_modified').val('');
        $('#to_modified').val('');
        $scope.formSearch.from_modified.$error.pattern = false;
        $scope.formSearch.to_modified.$error.pattern = false;

    }

    function convertSearchTitle(field, str) {
        var str = str.trim();
        str = str.replace(/\s+/g, " ");
        var array = str.split(" ");
        var query = '';
        if (array.length == 1) {
            query = (field + ' contains ' + "\'" + array[0] + "*\'").toString();
            query += ' OR ' + field + " = '" + str +"'";
        } else {
            for (var i = 0; i < array.length; i++) {
                query += (field + ' contains ' + "\'" + array[i] + "*\'" + ' AND ').toString();
            }
            query = query.substring(0, query.lastIndexOf('AND'));
            query += ' OR ' + field + " = '" + str +"'";
        }
        return query;
    }

    $scope.createQuery = function(titles, paths, option_1, from_created, to_created, option_2, from_modified, to_modified) {
        var query = "";

        if (titles != "") {
            query += convertSearchTitle("title", titles) + " AND ";
        }
        if (paths != "") {
            query += convertSearchTitle("path", paths) + " AND ";
        }            
        
        if (option_1 != "") {
            if (option_1 == "between") {
                if (from_created != "" && to_created != "") {
                    query += " created >= " + from_created + " AND created <= " + to_created + " AND ";
                } else {
                    //return "";
                }
            } else {
                if (from_created != "") {
                    if(option_1 != "="){
                        query += " created " + option_1 + from_created + " AND ";
                    }else{
                     var from_created2 = Date.parse($("#from_created").val() + ' 23:59:59');
                        query += " created >= " + from_created + " AND created <= " + from_created2 + " AND ";
                    }
                } else {
                   // return "";
                }
            }
        }

        if (option_2 != "") {
            if (option_2 == "between") {
                if (from_modified != "" && to_modified != "") {
                    query += " modified >= " + from_modified + " AND modified <= " + to_modified + " AND ";
                } else {
                   //return "";
                }
            } else {
                if (from_modified != "") {
                    if(option_2 != "="){
                        query += " modified " + option_2 + from_modified + " AND ";
                    }else{
                     var from_modified2 = Date.parse($("#from_modified").val() + ' 23:59:59');
                        query += " modified >= " + from_modified + " AND modified <= " + from_modified2 + " AND ";
                    }
                } else {
                    //return "";
                }
            }
        }
        query = query.substring(0, query.lastIndexOf('AND'));

        return query;
    }

    $scope.option_2 = "";
    $scope.option_1 = "";
    $scope.titles = "";
    $scope.paths = "";

    $scope.searchData = function(collection) {
       
        var from_created = '';
        var from_modified = '';
        var to_created = '';
        var to_modified = '';

        if ($scope.option_1 == "<=") {
            from_created = Date.parse($("#from_created").val() + ' 23:59:59');
        } else {
            if ($("#from_created").val() != "" && $("#from_created").val() != undefined) {
                from_created = Date.parse($("#from_created").val());
            }            
        }
        
        if ($scope.option_2 == "<=") {
            from_modified = Date.parse($("#from_modified").val() + ' 23:59:59');
        } else {
            if ($("#from_modified").val() != "" && $("#from_modified").val() != undefined) {
                from_modified = Date.parse($("#from_modified").val());
            }
        }
        
        if ($("#to_created").val() != "" && $("#to_created").val() != undefined) {
            to_created = Date.parse($("#to_created").val() + ' 23:59:59');
        }
        
        if ($("#to_modified").val() != "" && $("#to_modified").val() != undefined) {
            to_modified = Date.parse($("#to_modified").val() + ' 23:59:59');
        }

        if ($scope.titles != "" || $scope.paths != "" || $scope.option_1 != "" || $scope.option_2 != "" || from_created != "" || from_modified != "" || to_created != "" || to_modified != "") {
            $('#messNotFound').css("display", "none");
            var query = $scope.createQuery($scope.titles, $scope.paths, $scope.option_1, from_created, to_created, $scope.option_2, from_modified, to_modified);
            console.log("query =" + query);
            if(query != ""){
               // $('#workingSearch').show();
                 $scope.menus = {};
                var options = {
                    method: 'GET',
                    type: collection,
                    qs: {
                        ql: query
                    } //qs:{ql:"title contains 'f*'"}
                };
                $scope.init(options);
                // client.createCollection(options, function(err, data) {
                //     if (err) {
                //         $('#workingSearch').hide();
                //         $scope.openAlert("error", "", data.error_description);
                //     } else {
                //         $scope.menus = data._list;
                //         $scope.$apply($scope.menus);
                //         $('#workingSearch').hide();
                //         if (data._list.length <= 0) {
                //             $('#messNotFound').css("display", "block");
                //         }
                //     }
                // });
            }else{
                $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
            }
            
        } else {
            $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
        }
    }

} // End Menu List controller


var showMessCom = function(element, mess) {
    $(element).html(mess);
    $(element).css("display", "block");
}