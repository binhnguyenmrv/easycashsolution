'use strict';

/* Controllers */

var client = new Usergrid.Client({
  orgName: "binhnguyen",
  appName: "magrabbit"
});

if (!window.location.origin) {
  window.location.origin = window.location.protocol + "//" + window.location.host;
}

var current_user;
// Login controller

function LoginController($scope, $cookieStore, $rootScope) {
  $scope.usernameRegex = /^\w*$/;

  document.title = "Login";
  $scope.remember = false;
  $scope.current_user = $cookieStore.get('current_user');

  if ($scope.current_user === null || $scope.current_user === undefined) {
    location.href = window.location.origin + "/#/login";
    // location.href ="localhost:8000/#/login";
  } else {
    location.href = window.location.origin + "/#/admin";
    //location.href ="localhost:8000/#/admin";
  }

  $scope.isRegister = function() {
    location.href = window.location.origin + "/#/register";
  }
  $scope.getResponse = function() {
    function getBinary(file) {
      var xhr = new XMLHttpRequest();
      xhr.open("GET", file, false);
      xhr.overrideMimeType("text/plain; charset=x-user-defined");
      xhr.send(null);
      return xhr.responseText;
    }
    console.log(getBinary('http://gravatar.com/avatar'));

    function base64Encode(str) {
      var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
      var out = "",
        i = 0,
        len = str.length,
        c1, c2, c3;
      while (i < len) {
        c1 = str.charCodeAt(i++) & 0xff;
        if (i == len) {
          out += CHARS.charAt(c1 >> 2);
          out += CHARS.charAt((c1 & 0x3) << 4);
          out += "==";
          break;
        }
        c2 = str.charCodeAt(i++);
        if (i == len) {
          out += CHARS.charAt(c1 >> 2);
          out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
          out += CHARS.charAt((c2 & 0xF) << 2);
          out += "=";
          break;
        }
        c3 = str.charCodeAt(i++);
        out += CHARS.charAt(c1 >> 2);
        out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
        out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
        out += CHARS.charAt(c3 & 0x3F);
      }
      return out;
    }

    $('#asd').html('<img src="data:image/jpg;base64,' + base64Encode(getBinary('http://gravatar.com/avatar')) + '" />');
  };
  $scope._login = function(_username, _password) {
    $cookieStore.remove('user');
    $('#working').show();
    console.log("login = " + _username);
    var username = _username;
    var password = _password;

    client.login(username, password, function(err, data, user) {
      if (err) {
        //$scope.openAlert("error","","Invalid username or password.");
        $('#working').hide();
        $scope.openAlert("error", "", data.error_description);
      } else {
        current_user = user;
        // $cookieStore.put('current_user',username);
        var checkAdmin = false;
        var options = {
          type: 'users/' + username + '/groups'
        };
        client.createCollection(options, function(err, data) {
          if (err) {
            $scope.openAlert("error", "", "No Permission.");
          } else {
            if (data._list.length != 0) {
              while (data.hasNextEntity()) {
                var obj = data.getNextEntity();
                console.log(obj.get('path'));
                if ('admin' == obj.get('path')) {
                  checkAdmin = true;
                }
              }
              if (checkAdmin) {
                $cookieStore.put('user',user);
                $cookieStore.put('current_user', username);
                $('#working').hide();
                location.href = window.location.origin + "/#/admin";
              } else {
                $scope.openAlert("error", "", "No Permission.");
              }
            } else { //data.length ==0
              $scope.openAlert("error", "", "No Permission.");
            }
          }
        }); //End request get group
        $('#working').hide();

        if ($scope.remember) {
          setCookie("username", username, 365);
          setCookie("password", password, 365);
        } else {
          deleteCookie("username");
          deleteCookie("password");
        }
        //location.href =window.location.origin +"/#/admin";
        // //location.href ="localhost:8000/#/admin";
      }
    });
  }

  function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
      c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
      c_value = null;
    } else {
      c_start = c_value.indexOf("=", c_start) + 1;
      var c_end = c_value.indexOf(";", c_start);
      if (c_end == -1) {
        c_end = c_value.length;
      }
      c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
  }

  function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
  }

  function deleteCookie(c_name) {
    var cookie_date = new Date(); // current date & time
    cookie_date.setTime(cookie_date.getTime() - 1);
    document.cookie = c_name += "=; expires=" + cookie_date.toGMTString();
  }
  $scope.checkCookie = function() {
    var username = getCookie("username");
    var password = getCookie("password");

    if (username != null && username != "" && password != null && password != "") {
      $scope.username = username;
      $scope.password = password;
      $scope.remember = true;
    } else {
      $scope.username = "";
      $scope.password = "";
      $scope.remember = false;
    }
  }

  // //ALERT
  // $scope.openAlert = function(type, title, message){
  //     $('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');
  //     setTimeout(function(){$('#alertMsg').hide('slow')},3000);
  //   };
  //   $scope.closeAlert = function(){
  //     $('#alertMsg').hide();
  // };

} // end login controller
//--------------------------------REGISTER CONTROLLER

function RegisterController($scope) {
  $scope.username = "";
  $scope.email = "binh@gmail.com";
  $scope.fullname = "";
  $scope.password = "";
  $scope.repassword = "";
  $scope.activated = "true";
  $scope.submit = function() {
    console.log("username = " + $scope.username);
    var options = {
      method: 'POST',
      endpoint: "users",
      body: {
        name: $scope.fullname,
        username: $scope.username,
        email: $scope.email,
        password: $scope.password,
        activated: $scope.activated
      }
    };

    if ($scope.password === $scope.repassword) {
      client.request(options, function(err, data) {
        if (typeof(callback) === 'function') {
          callback(err, data);
        }
        location.href = window.location.origin + "/#/login";
        //location.href ="localhost:8000/#/books";
      });
    } else {
      alert("Error");
    }
  }
}
//----------------------------------END REGISTER CONTROLLER
// User controller
//var sdk = require('usergrid-sdk');

function BookController($scope, $http, $cookieStore) {
  $scope.books = [];
  $scope.uuid = "";
  $scope.bookName = "";
  $scope.author = "";
  $scope.bookObject = {};
  $scope.current_user = $cookieStore.get('current_user');
  $scope.getBooks = function() {
    console.log(current_user);
    var url = "";
    if (current_user != null) {
      url = "users/" + current_user._data.name + "/has/books";
      var options = {
        method: 'GET',
        endpoint: url
      }
      client.request(options, function(err, data) {
        if (err) {
          $("#msg").html('<pre>ERROR: ' + data + '</pre>');
        } else {
          $scope.books = data.entities;
          $scope.$apply($scope.books);
        }
      });
    } else {
      url = "https://api.usergrid.com/binhnguyen/magrabbit/books";
      $http.get(url).success(function(data) {
        $scope.books = data.entities;
      });
    }
    var output = JSON.stringify($scope.books, null, 2);
    $("#response").html('<pre>' + output + '</pre>');

    $scope.dogs = new Usergrid.Collection({
      "client": client,
      "type": "books"
    });

    $scope.dogs.fetch(function() {
      display();
    }, function() {
      alert('error');
    });
  }

  var display = function() {
    $('#next-button').hide();
    $('#previous-button').hide();
    $('#response').empty();
    while ($scope.dogs.hasNextEntity()) {
      var dog = $scope.dogs.getNextEntity();
      $('#response').append('<li>' + dog.get('name') + '</li>');
    }
    if ($scope.dogs.hasNextPage()) {
      $('#next-button').show();
    }
    if ($scope.dogs.hasPreviousPage()) {
      $('#previous-button').show();
    }
  }

  $scope.addBook = function(bookName, author) {
    console.log("bookName = " + bookName);
    console.log("author = " + author);
    var options = {
      type: "books"
      //qs:{ql:'order by title desc'}
    };

    client.createEntity(options, function(error, obj) {
      if (error)
        alert("Error!!!");
      else {
        obj.set("name", bookName);
        obj.set("author", author);
        obj.save();
        var endpoint = current_user.type + '/' + current_user.name + '/' + "has" + '/' + obj.get("type") + '/' + obj.get("name");
        var options = {
          method: 'POST',
          endpoint: endpoint
        };
        client.request(options, function(err, data) {
          if (err && client.logging) {
            console.log('entity could not be connected');
          }
          if (typeof(callback) === 'function') {
            callback(err, data);
          }
          $scope.getBooks();
        });
      }
    });
  }

  $scope.deleteBook = function(book) {
    console.log(book.uuid);
    var options = {
      method: 'DELETE',
      endpoint: "books/" + book.uuid
    }
    client.request(options, function(err, data) {
      if (err) {
        $("#msg").html('<pre>ERROR: ' + data + '</pre>');
      } else {
        $("#msg").html('<pre>' + book.name + ' has deleted :d</pre>');
        $scope.getBooks();
      }
    });
  }

  $scope.selectedBook = function(book) {
    $scope.uuid = book.uuid;
    $scope.bookName = book.name;
    $scope.author = book.author;
    $scope.bookObject = book;
  }

  $scope.prev = function() {
    $scope.dogs.getPreviousPage(function(err) {
      if (err) {
        alert("ERROR");
      } else {
        display();
      }
    });
  }
  $scope.next = function() {
    $scope.dogs.getNextPage(function(err) {
      if (err) {
        alert("ERROR");
      } else {
        display();
      }
    });
  }

  $scope.updateBook = function(bookName, author, uuid) {
    var options = {
      method: 'PUT',
      endpoint: 'books/' + uuid,
      body: {
        author: author
      }
    };
    client.request(options, function(err, data) {
      if (err) {
        alert("Error");
      } else {
        $scope.getBooks();
      }
    });
  }
} // end Book controller

//test for service

function TestController($scope) {
  $scope.isDisabled = false;
  $scope.names = ['igor', 'misko', 'vojta'];
  $scope.template = "partials/template1.html";
  $scope.template2 = "partials/template2.html";

  $scope.list = [];
  $scope.text = 'hello';
  $scope.submit = function() {
    if (this.text) {
      this.list.push(this.text);
      this.text = '';
    }
  }

  $scope.numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  $scope.limit = 3;

} // end Test controller



//test for service

function HelloCtrl($scope, testService, testFactory) {
  $scope.name = "Binh Nguyen";
  $scope.fromService = testService.sayHello($scope.name);
  $scope.fromFactory = testFactory.sayHello("World");
  $scope.fromService1 = testService.sayGoodbye($scope.name);
  $scope.fromFactory1 = testFactory.sayGoodbye("World");
}

$(document).ready(function() {
  if (current_user != null) {
    $('#username').text = current_user.name;
    console.log(current_user.name);
  }

  var click = function() {
    console.log("fdsafdsa");
  }
  $('#aaa').click(function() {
    alert('jfhjdd');

  });
});