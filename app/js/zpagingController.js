//==========GROUP CONTROLLER==========
function zpagingController($scope){
	document.title = 'Group Management';
	$scope.path   = "";
	$scope.title  = "";
	$scope.groups = [];
	$scope.alertMsg ="";
	//Regular Expression
	$scope.titleRegex = /^\w*$/;
	$scope.pathRegex = /^\w*$/;
	
	

    //Filter
    $scope.search='';
	$scope.filterByPath = function(data){
		return data._data.path.toLowerCase().search($scope.search.toLowerCase()) != -1;
	}
    
	//ALERT
	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},3000);
  	};
	// //Get all group

	//Pagination augularjs
	$scope.pages =[];
    $scope.currentPage = 0;
    $scope.pageSize = 3;
    $scope.numberOfPages=0;
    $scope.totalRecord=0;

    $scope.paging = function(index){
  		$scope.currentPage=Math.ceil(index);
  	}
  	
	var options = {
	    type: "groups",
	    qs:{limit: 999,ql:"order by path"}
	};
	$scope.init = function(){
		$scope.groups = [];
		$scope.pages=[];
		client.createCollection(options, function(err, data){
			if(err)
				console.log("Could not get groups");
			else{
				$scope.groups= data._list;
				$scope.$apply($scope.groups);
				$scope.totalRecord = $scope.groups.length;
				$scope.numberOfPages=Math.ceil($scope.groups.length/$scope.pageSize);                
				// $scope.$apply($scope.numberOfPages);
				$scope.$apply($scope);

				for(var i =1 ; i<=$scope.numberOfPages;i++){
					$scope.pages.push(i);
				}		
				$scope.$apply($scope.pages);
				$('#myModal').hide();
			};//End else
		});
	};
	// //Get all user
 //    var options={
 //        type: "groups",
 //        qs:{ql:"order by path"}
 //    };
 //    $scope.init = function(){
 //    	$('#myModal').show();
 //        $scope.groups = [];
 //        var data;
 //        client.createCollection(options, function(err, data){
 //            if(err){            	
 //                $('#myModal').hide();
 //                $scope.openAlert("error","", "Could not get group from server.");
 //            }else{
 //                $scope.drawGroup = function(){
 //                    data.fetch(function(err, d){
 //                        $('#prevPage').hide();
 //                        $('#nextPage').hide();

 //                        //reset the pointer
 //                        data.resetEntityPointer();
 //                        $scope.groups= data._list;
 //                        $scope.$apply($scope.groups);                        

 //                        //if(data.hasNextPage() && data._previous.lenght!=0)
 //                        if(data.hasNextPage())
 //                        	$('#nextPage').show();

 //                        if(data.hasPreviousPage())
 //                            $('#prevPage').show();

 //                        console.log(data);
 //                        $('#myModal').hide();
 //                    });
 //                } //End drawGroup function

 //                $scope.prevPage = function(){
 //                    data.getPreviousPage(function(err, data){
 //                        if(err)
 //                            alert("err");
 //                        else
 //                            $scope.drawGroup();
 //                    });
 //                }
 //                $scope.nextPage = function(){
 //                    data.getNextPage(function(err, data){
 //                        if(err)
 //                            alert("err");
 //                        else
 //                            $scope.drawGroup();
 //                    });
 //                };
 //                $scope.drawGroup();
 //            };//End else
 //        });
 //    };

	//Initializing a pop-up
	$scope.opts = {
	    backdropFade: true,
	    dialogFade:true
  	};
	// "Create new group" pop-up appears
	$scope.open = function(){
		$scope.path="";
		$scope.title="";
		$scope.shouldBeOpen = true;
	}

	// "Create new group" pop-up disappears
	$scope.close = function(){
		$scope.shouldBeOpen = false;
	}
	//DELETE GROUP
	$scope.deleteGroup = function(){
		var array = [];
		//get all checked checkbox
		$(".chk:checked").each(function(){
		    array.push( $(this).val());
		});
		if(array.length<=0)
			$scope.openAlert("error","","Please select at least a row to delete.");
		else
			if (confirm('Are you sure you want to delete?')){				
				for(var i =0; i< array.length; i++){
					var endpoint='groups/' + array[i] ;
					var options={
						method:'DELETE',
						endpoint:endpoint
					};
					client.request(options, function(err, data){
						if(err)
							alert('Could not delete group!');
						else{
							location.reload();
						}
					});
				}//End for
			}//End confirm
	};

	//Create group function
	$scope.createGroup = function(){
		var options ={
			method : "POST",
			endpoint: "groups",
			body: {
				path : $scope.path,
				title : $scope.title
			}
		};
		$scope.shouldBeOpen = false;
		client.request(options, function(err, data){
			if(err)
				//$scope.openAlertFail();
				$scope.openAlert("error","","Could not create a group.");
			else{
				$scope.init();
				//$scope.openAlertSuccess();
				$scope.openAlert("success","Success!",$scope.path+" group has been created.");
			}
		});
	}

	//Navigate to Group detail page
	$scope.navDetail = function(group_uuid){
		location.href = "#/admin/groups/" +  group_uuid;
	};

	//Sort
	$scope.predicate = 'path';
  	$('.sort').click(function(){
  		if($scope.reverse){
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9650;</span>');}
  		else{
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9660;</span>');}
  	});
  	$scope.check = true;
  	$scope.checkAll = function(data){
  		console.log(data)
  		if(data)
  		$('input.chk').prop('checked', true);
  		else
  		$('input.chk').prop('checked', false);
  	}
  	$scope.check1 = function(){
  		var d=false;
  		$("input.chk:not(:checked)").each(function() {
                  d=true;
        });
        if(d){
       		 $('input.checkAll').prop('checked', false);
       		 $scope.check = true;
        }
        else{
        	$('input.checkAll').prop('checked', true);
        	$scope.check = false;
        }
  	}
};
//==========END GROUP CONTROLLER==========
