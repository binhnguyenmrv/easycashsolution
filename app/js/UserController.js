//=========USER CONTROLLER=========

function UserController($scope, $rootScope) {
	//Set title to page
	document.title = 'User Management';

	//Initializing information about User
	$scope.username = "";
	$scope.name = "";
	$scope.email = "";
	$scope.password = "";
	$scope.repassword = "";
	$scope.users = [];
	$scope.select_2 = "";
	$scope.select_1 = "";
	$rootScope.header_title ="User Management";
	//Regular Expression
	$scope.usernameRegex = /^\w*$/;
	$scope.nameRegex = /^([0-9a-zA-Z ])*$/;

    $(document).on("keypress", ".inputPass", function(key) {
		 if(key.charCode === 32){
                return false;
            }
	});
	$(document).on("keypress", ".inputRePass", function(key) {
		 if(key.charCode === 32){
                return false;
            }
	});
	//Get all user
	$scope.options = {
		type: "users",
		qs: {
			ql: "order by username",
		}
	};
	$scope.init = function(options) {
		$('input.checkAll').prop('checked', false);
		$('#myModal').show();
		$scope.users = [];
		var data;
		client.createCollection(options, function(err, data) {
			if (err) {
				$('#myModal').hide();
				$scope.openAlert("error", "", "Could not get user.");
			} else {
				$scope.drawListUser = function() {

					data.fetch(function(err, d) {
						$('#prevPage').hide();
						$('#nextPage').hide();

						//reset the pointer
						data.resetEntityPointer();
						$scope.users = data._list;
						$scope.$apply($scope.users);
						if (data._list.length <= 0) {
                            $('#messNotFound').css("display", "block");
                        }
                        else
                        	$('#messNotFound').css("display", "none");
						//if(data.hasNextPage() && data._previous.lenght!=0)
						if (data.hasNextPage())
							$('#nextPage').show();

						if (data.hasPreviousPage())
							$('#prevPage').show();
						$('#myModal').hide();
						$('#span3').height($('#span9').height());
					});
				} //End drawListUser function

				$scope.prevPage = function() {
					$('#myModal').show();
					data.getPreviousPage(function(err, data) {
						if (err) {
							$scope.openAlert("error", "", "Could not get previous page.");
						} else {
							$scope.drawListUser();
						}
					});

				}
				$scope.nextPage = function() {
					$('#myModal').show();
					data.getNextPage(function(err, data) {
						if (err) {
							$scope.openAlert("error", "", "Could not get next page.");
						} else {
							$scope.drawListUser();
						}
					});
				};
				$scope.drawListUser();
			}; //End else
		}); //end request
	}; //end init function

	//Initializing a pop-up
	$scope.opts = {
		backdropFade: true,
		dialogFade: true,
		keyboard: false,
		backdropClick: false
	};

	//"Create new user" pop-up appears
	$scope.open = function() {
		$scope.username = "";
		$scope.name = "";
		$scope.email = "";
		$scope.password = "";
		$scope.repassword = "";
		$scope.closeAlert();
		$scope.shouldBeOpen = true;
	};

	//Delete users function
	$scope.delete = function() {
		//get all checked checkbox
		var array = [];
		$(".chk:checked").each(function() {
			array.push($(this).val());
		});
		if (array.length <= 0){
			$scope.openAlert("error", "", "Please select at least a row to delete.");
		 	$('html, body').animate({scrollTop: '0px'}, 800);
		}else {
			if (confirm($rootScope.DELETE_CONFIRM)) {
				$('#working').show();
				for (var i = 0; i < array.length; i++) {
					var endpoint = 'users/' + array[i];
					var options = {
						method: 'DELETE',
						endpoint: endpoint
					};
					client.request(options, function(err, data) {
						if (err)
							$scope.openAlert("error", "", "Could not delete user.");
						else {
							$scope.openAlert("success", "", "Deleted Successfully.");
							$scope.init($scope.options);
						}
					});
				} //end for
				$('#working').hide();
			} //end confirm
		}
	};

	//Create new user function
	$scope.createUser = function() {
		$('#working').show();
		$scope.shouldBeOpen = false;
		var options = {
			method: "POST",
			endpoint: "users",
			body: {
				username: $scope.username,
				name: $scope.name,
				email: $scope.email,
				password: $scope.password
			}
		};
		client.request(options, function(err, data) {
			if (err) {
				$('#working').hide();
				$scope.openAlert("error", "", data.error_description);
			} else {
				$scope.init($scope.options);
				$('#working').hide();
				$scope.openAlert("success", "", $scope.CREATE_SUCCESS);
			}
		});
	};

	//Navigate to User detail page
	$scope.navDetail = function(user_uuid) {
		// $scope.uuid = user_uuid;
		//console.log($scope.uuid);
		location.href = "#/admin/users/" + user_uuid;
	};

	//Check, Uncheck all checkbox
	$scope.check = false;
	$scope.checkAll = function() {
		if ($scope.check)
			$('input.chk').prop('checked', true);
		else
			$('input.chk').prop('checked', false);
	}
	$scope.checkItem = function() {
		var ok = false;
		$("input.chk:not(:checked)").each(function() {
			ok = true;
		});
		if (ok) {
			$('input.checkAll').prop('checked', false);
			$scope.check = false;
		} else {
			$('input.checkAll').prop('checked', true);
			$scope.check = true;
		}
	} //End check, uncheck checkbox

	//Sort
	$scope.predicate = 'username';
	$('.sort').click(function() {
		if ($scope.reverse) {
			$('#sortImg').remove();
			$(this).append('<span id="sortImg"> &#9650;</span>');
		} else {
			$('#sortImg').remove();
			$(this).append('<span id="sortImg"> &#9660;</span>');
		}
	});

	//SEARCH FUNCTION
	//----------SEARCH----------------
    $scope.changeSelect = function(type) {
        if (type == "created") {
            $('#from_created').val('');
            $('#to_created').val('');
            $scope.formSearch.from_created.$error.pattern = false;
            $scope.formSearch.to_created.$error.pattern = false;
        } else {
            $('#from_modified').val('');
            $('#to_modified').val('');
            $scope.formSearch.from_modified.$error.pattern = false;
            $scope.formSearch.to_modified.$error.pattern = false;
        }
    }

    $scope.resetSearch = function() {
        $scope.usernames = '';
        $scope.names = '';
        $scope.option_1 = "";
        $scope.option_2 = "";
        $('#from_created').val('');
        $('#to_created').val('');
        $scope.formSearch.from_created.$error.pattern = false;
        $scope.formSearch.to_created.$error.pattern = false;
        $('#from_modified').val('');
        $('#to_modified').val('');
        $scope.formSearch.from_modified.$error.pattern = false;
        $scope.formSearch.to_modified.$error.pattern = false;

    }

    function convertSearchTitle(field, str) {
        var str = str.trim();
        str = str.replace(/\s+/g, " ");
        var array = str.split(" ");
        var query = '';
        if (array.length == 1) {
            query = (field + ' contains ' + "\'" + array[0] + "*\'").toString();
            query += ' OR ' + field + " = '" + str +"'";
        } else {
            for (var i = 0; i < array.length; i++) {
                query += (field + ' contains ' + "\'" + array[i] + "*\'" + ' AND ').toString();
            }
            query = query.substring(0, query.lastIndexOf('AND'));
            query += ' OR ' + field + " = '" + str +"'";
        }
        return query;
    }

    $scope.createQuery = function(usernames, name, option_1, from_created, to_created, option_2, from_modified, to_modified) {
        var query = "";

        if (usernames != "") {
            query += "username = '" + usernames + "*' AND ";
        }
        if (name != "") {
            query += convertSearchTitle("name", name) + " AND ";
        }            
        
        if (option_1 != "") {
            if (option_1 == "between") {
                if (from_created != "" && to_created != "") {
                    query += " created >= " + from_created + " AND created <= " + to_created + " AND ";
                } else {
                    //return "";
                }
            } else {
                if (from_created != "") {
                    if(option_1 != "="){
                        query += " created " + option_1 + from_created + " AND ";
                    }else{
                     var from_created2 = Date.parse($("#from_created").val() + ' 23:59:59');
                        query += " created >= " + from_created + " AND created <= " + from_created2 + " AND ";
                    }
                } else {
                   // return "";
                }
            }
        }

        if (option_2 != "") {
            if (option_2 == "between") {
                if (from_modified != "" && to_modified != "") {
                    query += " modified >= " + from_modified + " AND modified <= " + to_modified + " AND ";
                } else {
                   //return "";
                }
            } else {
                if (from_modified != "") {
                    if(option_2 != "="){
                        query += " modified " + option_2 + from_modified + " AND ";
                    }else{
                     var from_modified2 = Date.parse($("#from_modified").val() + ' 23:59:59');
                        query += " modified >= " + from_modified + " AND modified <= " + from_modified2 + " AND ";
                    }
                } else {
                    //return "";
                }
            }
        }
        query = query.substring(0, query.lastIndexOf('AND'));

        return query;
    }

    $scope.option_2 = "";
    $scope.option_1 = "";
    $scope.usernames = "";
    $scope.names = "";

    $scope.searchData = function(collection) {
        var from_created = '';
        var from_modified = '';
        var to_created = '';
        var to_modified = '';

        if ($scope.option_1 == "<=") {
            from_created = Date.parse($("#from_created").val() + ' 23:59:59');
        } else {
            if ($("#from_created").val() != "" && $("#from_created").val() != undefined) {
                from_created = Date.parse($("#from_created").val());
            }            
        }
        
        if ($scope.option_2 == "<=") {
            from_modified = Date.parse($("#from_modified").val() + ' 23:59:59');
        } else {
            if ($("#from_modified").val() != "" && $("#from_modified").val() != undefined) {
                from_modified = Date.parse($("#from_modified").val());
            }
        }
        
        if ($("#to_created").val() != "" && $("#to_created").val() != undefined) {
            to_created = Date.parse($("#to_created").val() + ' 23:59:59');
        }
        
        if ($("#to_modified").val() != "" && $("#to_modified").val() != undefined) {
            to_modified = Date.parse($("#to_modified").val() + ' 23:59:59');
        }

        if ($scope.usernames != "" || $scope.names != "" || $scope.option_1 != "" || $scope.option_2 != "" || from_created != "" || from_modified != "" || to_created != "" || to_modified != "") {
            $('#messNotFound').css("display", "none");
            var query = $scope.createQuery($scope.usernames, $scope.names, $scope.option_1, from_created, to_created, $scope.option_2, from_modified, to_modified);
            console.log("query =" + query);
            if(query != ""){
        		$scope.users = {};
                var options = {
                    method: 'GET',
                    type: collection,
                    qs: {
                        ql: query
                    } //qs:{ql:"title contains 'f*'"}
                };
               	$scope.init(options);
            }else{
                $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
            }
            
        } else {
            $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
        }
    }
};
//=========END USER CONTROLLER=========

//=========USER DETAIL CONTROLLER=========

function UserDetailController($scope, $routeParams, $rootScope) {
	//Initializing a pop-up
	$scope.opts = {
		backdropFade: true,
		dialogFade: true,
		keyboard: false,
		backdropClick: false
	};
	//Set title to page
	document.title = 'Detail User';

	//Control 3 tab Profile, Groups, Roles
	$scope.showProfile = function() {
		$('#myTab #profiletab').tab('show');
	};
	$scope.showGroups = function() {
		$scope.initGroups();
		$('#myTab #groupstab').tab('show');
	};
	$scope.showRoles = function() {
		$scope.initRoles();
		$('#myTab #rolestab').tab('show');
	}; //End control tab

	/*Control Add, Remove, Select All, Deselect All item
		is_group = true	: used to Group
		is_group = false: used to Role
	*/
	//Add group or role to user function
	$scope.Add = function(select_name, is_group) {
		var arr = [];
		$('[name="' + select_name + '"] :selected').each(
			function(i, selected) {
				arr[i] = $(selected).text();

				if (is_group) {
					$scope.user_has_group.push(arr[i]);

					var index = $scope.groups_add_user.indexOf(arr[i]);
					$scope.groups_add_user.splice(index, 1);
				} else {
					$scope.role_of_user.push(arr[i]);

					var index = $scope.role_add_user.indexOf(arr[i]);
					$scope.role_add_user.splice(index, 1);
				}

			});
	};

	//Remove group or role to user function
	$scope.Remove = function(select_name, is_group) {
		var arr = [];
		$('[name="' + select_name + '"] :selected').each(
			function(i, selected) {
				arr[i] = $(selected).text();

				if (is_group) {
					$scope.groups_add_user.push(arr[i]);

					var index = $scope.user_has_group.indexOf(arr[i]);
					$scope.user_has_group.splice(index, 1);
				} else {
					$scope.role_add_user.push(arr[i]);

					var index = $scope.role_of_user.indexOf(arr[i]);
					$scope.role_of_user.splice(index, 1);
				}

			});
	};

	//Select All group or role to user function
	$scope.SelectAll = function(select_name, is_group) {
		var arr = [];
		$('[name="' + select_name + '"] option').each(
			function(i, selected) {
				arr[i] = $(selected).val();

				if (is_group) {
					$scope.user_has_group.push(arr[i]);

					var index = $scope.groups_add_user.indexOf(arr[i]);
					$scope.groups_add_user.splice(index, 1);
				} else {
					$scope.role_of_user.push(arr[i]);

					var index = $scope.role_add_user.indexOf(arr[i]);
					$scope.role_add_user.splice(index, 1);
				}
			});
	};

	//Deselect All group or role to user function
	$scope.DeselectAll = function(select_name, is_group) {
		var arr = [];
		$('[name="' + select_name + '"] option').each(
			function(i, selected) {
				arr[i] = $(selected).val();

				if (is_group) {
					$scope.groups_add_user.push(arr[i]);

					var index = $scope.user_has_group.indexOf(arr[i]);
					$scope.user_has_group.splice(index, 1);
				} else {
					$scope.role_add_user.push(arr[i]);

					var index = $scope.role_of_user.indexOf(arr[i]);
					$scope.role_of_user.splice(index, 1);
				}
			});
	}; //End control item

	//Get UUID from list user page
	$scope.uuid = $routeParams.userid;

	//Regular Expression
	$scope.telephoneRegex = /^([0-9])*$/;
	$scope.birthdayRegex = /^(((0[13578]|1[02])\/(0[1-9]|[12]\d|3[01])\/((19|[2-9]\d)\d{2}))|((0[13456789]|1[012])\/(0[1-9]|[12]\d|30)\/((19|[2-9]\d)\d{2}))|(02\/(0[1-9]|1\d|2[0-8])\/((19|[2-9]\d)\d{2}))|(02\/29\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/

	//----------1. PROFILE TAB -----------

	//Navigate to Users page
	$scope.cancel = function() {
		location.href = "#/admin";
	};

	//Initializing Profile
	$scope.initProfile = function() {
		$scope.user = {};
		$('#myModal').show();
		$('#myTab #profiletab').tab('show');
		var options = {
			method: "GET",
			endpoint: "users/" + $scope.uuid
		};
		client.request(options, function(err, data) {
			if (err)
				console.log("Could not get user");
			else {
				$scope.user = data.entities[0];
				$scope.original_name = $scope.user.name;
				$scope.image = {};
        		$scope.image.url = $scope.user.picture;
				$scope.$apply($scope.user);

		        $('#user_update').attr("disabled", "disabled");
				$('#myModal').hide();
			}
		});
	};

	//Update User function
	$scope.update = function(uuid, image) {
		var endpoint = "";  
        if(image != null && image != undefined && image.file != null && image.file != undefined){
            $rootScope.upload(image, function(){
                $scope.user.picture = $rootScope.uploadedImgSrc;
                console.log($rootScope.uploadedImgSrc);
                updateUser(uuid);
            });
        }else{
            updateUser(uuid);
        }
	};

	var updateUser = function(uuid){
		var options = {
			method: 'PUT',
			endpoint: 'users/' + uuid,
			body: {
				username: $scope.user.username,
				name: $scope.user.name,
				title: $scope.user.title,
				url: $scope.user.url,
				email: $scope.user.email,
				tel: $scope.user.tel,
				picture: $scope.user.picture,
				bday: $scope.user.bday
			}
		};
		client.request(options, function(err, data) {
			if (err)
				$scope.openAlert("error", "", "Could not update user.");
			else {
				$scope.initProfile();
				$scope.openAlert("success", "", "Updated Successfully.");
			}
		});
	}
	//---------- END PROFILE TAB -----------

	//----------2. GROUP TAB -----------

	//Initalizing Groups
	$scope.initGroups = function() {
		$('#working').show();
		$scope.user_has_group = [];
		$scope.groups_add_user = [];
		$scope.user_has_group_original = [];
		//List group of user
		options = {
			type: "users/" + $scope.uuid + "/groups"
		};
		client.createCollection(options, function(err, data) {
			if (err) {
				console.log("Could not get group!");
			} else {
				while (data.hasNextEntity()) {
					var ob = data.getNextEntity();
					$scope.user_has_group.push(ob.get("path"));
					$scope.user_has_group_original.push(ob.get("path"));
				};

				//Load list of group add to user
				var options = {
					type: "groups"
				};
				client.createCollection(options, function(err, data) {
					if (err)
						console.log("Could not get users");
					else {
						while (data.hasNextEntity()) {
							var obj = data.getNextEntity();
							if (!isExistGroup(obj.get("path")))
								$scope.groups_add_user.push(obj.get("path"));
						}
						$scope.$apply($scope);
					}
				}); //end request
				$('#working').hide();

			} //end else
		}); //end get list group of user
	};
	//Add/Remove group to user function
	$scope.updateGroup = function() {
		//Remove Group
		var arr_remove = [];
		//Get all Group removed and push to arr_remove array.
		$scope.user_has_group_original.forEach(function(value, index, array) {
			var check = $scope.user_has_group.indexOf(value);
			if (check == -1) {
				arr_remove.push(value);
			}
		});
		if (arr_remove.length != 0) {
			arr_remove.forEach(function(value, index, array) {
				//Update item for users_in_group_original array.
				var check = $scope.user_has_group_original.indexOf(value);
				$scope.user_has_group_original.splice(check, 1);

				//Executing REMOVE Group
				var options = {
					method: 'DELETE',
					endpoint: "users/" + $scope.uuid + "/groups/" + value
				};
				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not delete user.");
					} else {
						console.log("remove: " + value);
					}
				});
			});
			$scope.openAlert("success", "", "Updated Successfully.");
		}
		console.log(arr_remove);
		console.log($scope.user_has_group_original);

		//Add Group
		var arr_add = [];
		//Get all Group added and push to arr_add array.
		$scope.user_has_group.forEach(function(value, index, array) {
			var index = $scope.user_has_group_original.indexOf(value);
			if (index == -1) {
				arr_add.push(value);
			}
		});
		if (arr_add.length != 0) {
			arr_add.forEach(function(value, index, array) {
				//Update item for user_has_group_original array.
				$scope.user_has_group_original.push(value);

				//Executing ADD Group
				var options = {
					method: "POST",
					endpoint: "users/" + $scope.uuid + "/groups/" + value
				};

				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not add user.");
					} else {
						console.log("add: " + value);
					}
				});
			});
			$scope.openAlert("success", "", "Updated Successfully.");
		}
		console.log(arr_add);
		console.log($scope.user_has_group_original);
	};
	//Check exist group

	function isExistGroup(group) {
		for (var i = 0; i < $scope.user_has_group.length; i++) {
			if ($scope.user_has_group[i] == group) {
				return true;
			}
		}
		return false;
	};

	//---------- END GROUP TAB -----------

	//----------3. ROLES TAB---------

	//Initializing Roles
	$scope.initRoles = function() {
		$('#working').show();
		$scope.role_of_user = [];
		$scope.role_add_user = [];
		$scope.role_of_user_original = [];
		//List role of user
		options = {
			type: "users/" + $scope.uuid + "/roles"
		};
		client.createCollection(options, function(err, data) {
			if (err) {
				console.log("Could not get role of user");
			} else {
				while (data.hasNextEntity()) {
					var ob = data.getNextEntity();
					$scope.role_of_user.push(ob.get("roleName"));
					$scope.role_of_user_original.push(ob.get("roleName"));
				};

				//Load list of role add to user
				var options = {
					type: "roles"
				};
				client.createCollection(options, function(err, data) {
					if (err)
						console.log("Could not get roles");
					else {
						$scope.role_add_user = [];
						while (data.hasNextEntity()) {
							var obj = data.getNextEntity();
							if (!isExistRole(obj.get("roleName")))
								$scope.role_add_user.push(obj.get("roleName"));
						}
						$scope.$apply($scope);
						console.log($scope.role_add_user);
						console.log($scope.role_of_user);
					}
				}); //end request
				$('#working').hide();
			} //end else
		}); //end list role of user
	};
	//Add/Remove role to user function
	$scope.updateRole = function() {
		//Remove role
		var arr_remove = [];
		//Get all Role removed and push to arr_remove array.
		$scope.role_of_user_original.forEach(function(value, index, array) {
			var index = $scope.role_of_user.indexOf(value);
			if (index == -1) {
				arr_remove.push(value);
			}
		});
		if (arr_remove.length != 0) {
			arr_remove.forEach(function(value, index, array) {
				//Update item for role_of_user_original array
				var check = $scope.role_of_user_original.indexOf(value);
				$scope.role_of_user_original.splice(check, 1);

				//Executing REMOVE Role
				var options = {
					method: 'DELETE',
					endpoint: "users/" + $scope.uuid + "/roles/" + value
				};
				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not delete user.");
					} else {
						console.log("remove: " + value);
					}
				});
			});
			$scope.openAlert("success", "", "Updated Successfully.");
		}
		console.log(arr_remove);
		console.log($scope.role_of_user_original);

		//Add Role
		var arr_add = [];
		//Get all Role added and push to arr_add array.
		$scope.role_of_user.forEach(function(value, index, array) {
			var index = $scope.role_of_user_original.indexOf(value);
			if (index == -1) {
				arr_add.push(value);
			}
		});
		if (arr_add.length != 0) {
			arr_add.forEach(function(value, index, array) {
				//Update role_of_user_original array
				$scope.role_of_user_original.push(value);

				//Executing ADD Role
				var options = {
					method: "POST",
					endpoint: "users/" + $scope.uuid + "/roles/" + value
				};

				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not add user.");
					} else {
						console.log("add: " + value);
					}
				});
			});
			$scope.openAlert("success", "", "Updated Successfully.");
		}
		console.log(arr_add);
		console.log($scope.role_of_user_original);
	};

	//Check exist role

	function isExistRole(roleName) {
		for (var i = 0; i < $scope.role_of_user.length; i++) {
			if ($scope.role_of_user[i] == roleName) {
				return true;
			}
		}
		return false;
	};
	//----------END ROLES TAB---------
};
//=========END USER DETAIL CONTROLLER=========