'use strict';

/* Filters */

angular.module('ecsFilters', [])
    .filter('checkmark', function() {
        return function(input) {
            return input ? '\u2713' : '\u2718';
        };
    })
    .filter('reverse', function() {
        return function(input, uppercase) {
            var out = "";
            for (var i = 0; i < input.length; i++) {
                out = input.charAt(i) + out;
            }
            // conditional based on optional argument
            if (uppercase) {
                out = out.toUpperCase();
            }
            return out;
        }
    })
    .filter('boolConverter', function() {
        return function(input) {
            return input == 1 ? 'Published' : 'Draft';
        }
    })
    .filter('statusConverter', function() {
        return function(input) {
            return input == 'published' ? 'Published' : 'Draft';
        }
    })
    .filter('uppcaseFirstChar', function() {
        return function(input) {
            return input.charAt(0).toUpperCase() + input.slice(1);
        }
    })
    .filter('checkImage', function() {
        return function(url) {
            // $.ajax({
            //       url:url,
            //       type:'HEAD',
            //       error: function(data)
            //       {
            //           return "default"; // default
            //       },
            //       success: function(data)
            //       {
            //            return url;
            //       }
            //   });      
        }
    });