function BlogsController($scope, $cookieStore, $rootScope) {
	$scope.word1 = /^([0-9a-zA-Z ])*$/;
	document.title = 'Blog Management';
	current_user = $cookieStore.get("current_user");
	$scope.descriptions = "";
	$scope.status = "";
	$scope.title = "";
	$scope.categorie = ""
	$scope.category = ""
	$scope.search = '';
	$scope.cate = "";
	$scope.chEditor = true;
	$scope.status_selected = "";
	$scope.select_2 = "";
	$scope.select_1 = "";
	$rootScope.header_title ="Blog Management";
	//Clear variables after created blog
	$scope.clear = function() {
		$scope.cate = "0";
		$("input[name=title]").val('');
		$scope.newForm.$pristine = true;
		$scope.newForm.sel.$pristine = true;
		$scope.newForm.title.$pristine = true;
		$scope.newForm.title.$invalid = false;
		$scope.newForm.title.$dirty = false;
	}
	//Show popup new
	$('#show-modal').on("click", function() {
		$scope.clear();
		$('#working').show();
		$('#redactor1').redactor({
			minHeight: 200,
			maxWidth: 500
		});
		$(".redactor_editor").html('');
		$scope.initCategory();
		$("#newBlog").css({
			"top": "-50%"
		}).show().animate({
			"top": "50%"
		}, 300);
		$("body").append('<div class="modal-backdrop fade in"></div>');
		return false;
	});
	// When the editor loses the focus, tell the change event to fire so Lightswitch recognises that the value needs saving...
	$(document).on("keyup", "#new-textarea .redactor_editor", function() {
		$("#redactor1").trigger("change");
	});

	$(document).on("change", "#redactor1", function(data) {
		console.log($('#redactor1').val());
		$scope.chEditor = false;
		console.log($scope.chEditor);
	});

	$(document).on("keyup", "#edit_textarea .redactor_editor", function() {
		$("#redactor2").trigger("change");
	});

	$(document).on("change", "#redactor2", function(data) {
		$scope.changeForm($scope.editBlog);
	});
	//Close popup new
	$("#newBlog img[data-dismiss=modal]").on("click", function() {
		$("#newBlog").animate({
			"top": "-50%"
		}, 300, function() {
			$('.modal-backdrop').remove();
		});
		return true;
	});

	$("#newBlog button[data-dismiss=modal]").on("click", function() {
		$("#newBlog").animate({
			"top": "-50%"
		}, 300, function() {
			$('.modal-backdrop').remove();
		});
		return true;
	});
	//check Textarea
	$scope.dirty = true;
	$scope.ckTextarea = function() {
		$scope.dirty = false;
	}
	//Show popup edit
	$scope.editModal = function(data) {	
		$('#redactor2').redactor({
			minHeight: 200,
			maxWidth: 500
		});
		$("#edit_textarea .redactor_editor").html('');
		if(data != null)
			$("#edit_textarea .redactor_editor").html(data);
		$("#editBlog").css({
			"top": "-50%"
		}).show().animate({
			"top": "50%"
		}, 300);
		
		$("body").append('<div class="modal-backdrop fade in"></div>');

		$scope.myForm.$pristine = true;
		$scope.myForm.select.$pristine = true;
		$scope.myForm.title.$pristine = true;
		$scope.myForm.title.$invalid = false;
		$scope.myForm.title.$dirty = false;
		$scope.myForm.optionsRadios1.$pristine = true;
		$scope.myForm.optionsRadios2.$pristine = true;
		return false;
	}
	//Close popup edit
	$("#editBlog img[data-dismiss=modal]").on("click", function() {
		$("#editBlog").animate({
			"top": "-50%"
		}, 300, function() {
			$('.modal-backdrop').remove();
			$scope.dirty = true;
			$scope.$apply($scope.dirty);
			$('#btn-update').attr("disabled", "disabled");
			return false;
		});
	});

	$("button[data-dismiss=modal]").on("click", function() {
		$("#editBlog").animate({
			"top": "-50%"
		}, 300, function() {
			$('.modal-backdrop').remove();
			$scope.dirty = true;
			$scope.$apply($scope.dirty);
			$('#btn-update').attr("disabled", "disabled");
			return false;
		});
	});
	//Filter title in table blogs
	//  	$scope.filterByTitle = function(data){
	// 	return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
	// }
	// Get blogs from server
	$scope.options = {
		type: "blogs"
	};
	$scope.init = function(options) {
		$('input.checkAll').prop('checked', false);
		$('#myModal').show();
		$scope.contents = [];
		//$scope.initCategory();
		var data;
		client.createCollection(options, function(err, data) {
			if (err) {
				$('#myModal').hide();
				location.reload();
			} else {
				$scope.drawPages2 = function() {
					data.fetch(function(err, d) {
						$('#prevPage').hide();
						$('#nextPage').hide();
						//reset the pointer
						data.resetEntityPointer();
						$scope.contents = data._list;
						$scope.$apply($scope.contents);
						if (data._list.length <= 0) {
                            $('#messNotFound').css("display", "block");
                        }
                        else
                        	$('#messNotFound').css("display", "none");
						if (data.hasNextPage())
							$('#nextPage').show();
						if (data.hasPreviousPage())
							$('#prevPage').show();
						$('#myModal').hide();
						$('#working').hide();
						$('#span3').height($('#span9').height());
					});
				} //End drawUser function
				$scope.prevPage = function() {
					data.getPreviousPage(function(err, data) {
						if (err)
							alert("err");
						else
							$scope.drawPages2();
					});
				}
				$scope.nextPage = function() {
					data.getNextPage(function(err, data) {
						if (err)
							alert("err");
						else
							$scope.drawPages2();
					});
				};
				$scope.drawPages2();
			}; //End else
		});
	};
	//Get categories from server
	var options1 = {
		method: "GET",
		endpoint: "categories/",
		qs:{limit:99}
	}
	$scope.initCategory = function() {
		$scope.categories = [];
		$scope.cate = "";
		client.request(options1, function(err, data) {
			if (err) {
				//console.log("Could not get user");
				//alert('could not get categories from server');
				$scope.openAlert1("error", "", "Could not get categories.");
				location.reload();
			} else {
				$scope.categories = data.entities;
				$scope.$apply($scope.categories);
				$('#working').hide();
			}
		});
	}

	function getCategory(){
		$scope.categories = [];
		$scope.cate = "";
		client.request(options1, function(err, data) {
			if (err) {
				//console.log("Could not get user");
				//alert('could not get categories from server');
				$scope.openAlert1("error", "", "Could not get categories.");
				location.reload();
			} else {
				$scope.categories = data.entities;
				$scope.$apply($scope.categories);
				$('#working').hide();
			}
		});
	}
	//Function create blog
	$scope.createBlogs = function() {
		console.log($scope.cate);
		$('#working').show();
		var name = $scope.title.replace(/\s/g, "");
		var category;
		$.each($scope.categories,function(key,value){
			if($scope.cate == value.uuid){
				category = value.title;
			}
		});
		console.log(category);
		var descriptions = '';
		if (null != $('#redactor1').val())
			descriptions = $('#redactor1').val();
		else descriptions = "''";
		$(".chke:checked").each(function() {
			$scope.status = $(this).val();
		});
		var options = {
			method: 'POST',
			endpoint: 'blogs',
			body: {
				name: name,
				title: $scope.title,
				author: current_user,
				status: $scope.status,
				descriptions: descriptions,
				categories: $scope.cate,
				categoryName: category
			}
		};
		client.request(options, function(err, obj) {
			if (err) {
				$scope.openAlert("error", "", "Could not create blog");
				$('#working').hide();
			} else {
				var endpoint = "categories" + '/' + $scope.cate + '/' + "has" + '/' + "blogs" + '/' + name;
				var options2 = {
					method: 'POST',
					endpoint: endpoint
				};
				client.request(options2, function(err, data) {
					if (err) {
						$('#working').hide();
						$rootScope.openAlert1("error", "", "Could not create relationship.");
					} else {
						$('#working').hide();
						$scope.init($scope.options);
						$scope.openAlert("success", "", $rootScope.CREATE_SUCCESS);
						$("#newBlog").animate({
							"top": "-50%"
						}, 500, function() {
							$('.modal-backdrop').remove();
						});
					} // ense else
				});
			} // end else
		});
	} // end function createBlogs
	// Function delete

	$scope.delete = function() {
		$scope.array = [];
		$(".chk:checked").each(function() {
			$scope.array.push($(this).val());
		});
		if ($scope.array.length == 0) {
			$scope.openAlert("error", "", "Please select at least a row to delete.");
			$('html, body').animate({scrollTop: '0px'}, 800);
		} else
		if (confirm($rootScope.DELETE_CONFIRM)) {
			$('#myModal').show();
			for (var i = 0; i < $scope.array.length; i++) {
				var endpoint = 'blogs/' + $scope.array[i];
				var options = {
					method: 'DELETE',
					endpoint: endpoint
				};
				client.request(options, function(err, data) {
					if (err) {
						$scope.openAlert("error", "", "Could not delete blog.");
					} else {
						$('#myModal').hide();
						$scope.init($scope.options);
						$scope.openAlert("success", "", $rootScope.DELETE_SUCCESS);
						$('input.checkAll').prop('checked', false);
						$scope.check = true;
					}
				});
			}
		}
	};
	// Sort and check checkbox
	$scope.predicate = 'title';
	$('.sort').click(function() {
		if ($scope.reverse) {
			$('#sortImg').remove();
			$(this).append('<span id="sortImg"> &#9650;</span>');
		} else {
			$('#sortImg').remove();
			$(this).append('<span id="sortImg"> &#9660;</span>');
		}
	});
	$scope.checkDelete = true;
	$scope.check = true;
	$scope.checkAll = function(data) {
		if (data)
			$('input.chk').prop('checked', true);
		else
			$('input.chk').prop('checked', false);
	}
	$scope.check1 = function() {
		var d = false;
		$("input.chk:not(:checked)").each(function() {
			d = true;
		});
		if (d) {
			$('input.checkAll').prop('checked', false);
			$scope.check = true;
		} else {
			$('input.checkAll').prop('checked', true);
			$scope.check = false;
		}
	}
	//Edit blog
	var old_category = "";
	var old_categoryuuid = "";
	var old_obj = null;
	$scope.selectedetail = {};
	$scope.categorydetail = null;
	$scope.categoryuuiddetail = null;
	$scope.contentsdetail = {};
	$scope.statusdetail = "";
	//$scope.checkTextarea = true;
	// CKEDITOR.instances['editor'].on('change', function() {
	// 	$scope.checkTextarea = false;
	// 	$scope.$apply($scope.checkTextarea);
	// });
	// function edit blogs
	$scope.openDetail = function(data) {
		$('#working1').show();
		old_obj = data._data;
		$scope.categoriesdetail = [];
		$scope.initCategoryEdit(data._data.categories);
		$scope.editModal(data._data.descriptions);
		$scope.editBlog = JSON.parse(JSON.stringify(data._data));
	}
	$scope.changeForm = function(obj) {
		obj.categories = $scope.select.uuid;
		obj.descriptions = $('#redactor2').val();
		console.log("" + JSON.stringify(obj.descriptions));
		console.log("" + JSON.stringify(old_obj.descriptions));
		if(old_obj.descriptions == null){
			old_obj.descriptions = '<p></p>';
		}
		var b = angular.equals(old_obj, obj);
		if (b) {
			$('#btn-update').attr("disabled", "disabled");
		} else {
			$('#btn-update').removeAttr("disabled", "disabled");
		}
	};
	// get categories from server
	$scope.initCategoryEdit = function(categories) {
		var options1 = {
			method: "GET",
			endpoint: "categories/",
			qs:{limit:99}
		}
		client.request(options1, function(err, data) {
			if (err) {
				//alert('Could not get categories from server');
				$scope.openAlert1("error", "", "Could not get categories.");
				location.reload();
			} else {
				$scope.categoriesdetail = data.entities;
				$.each(data.entities, function(key, value) {
					if (value.uuid == categories) {
						$scope.select = data.entities[key];
						$scope.$apply($scope.select);
						old_categoryuuid = data.entities[key].uuid;
						return;
					}
				});
				$scope.$apply($scope.categoriesdetail);
				$('#working1').hide();
			}
		});
	} // end initCtegoryEdit

	//Edit blog function
	$scope.update = function() {
		$('#working1').show();
		if (old_categoryuuid != $scope.select.uuid) { // if change category
			var endpoint = 'categories/' + old_categoryuuid + '/has/blogs/' + old_obj.uuid;
			var options = {
				method: 'DELETE',
				endpoint: endpoint
			};
			client.request(options, function(err, data) {
				if (err) {
					$('#working1').hide();
					$scope.openAlert("error", "", "Could not update");
					$("#editBlog").animate({
						"top": "-50%"
					}, 500, function() {
						$('.modal-backdrop').remove();
					});
				}
			});
		}
		var descriptions = $('#redactor2').val();
		$(".chked:checked").each(function() {
			$scope.editBlog.status = $(this).val();
		});
		var options = {
			method: 'PUT',
			endpoint: "blogs/" + old_obj.uuid,
			body: {
				title: $scope.editBlog.title,
				author: current_user,
				status: $scope.editBlog.status,
				descriptions: descriptions,
				categories: $scope.select.uuid
			}
		};
		client.request(options, function(err, data) {
			if (err && client.logging) {
				$scope.openAlert1("error", "", "Could not update blog.");
			} else {
				var endpoint = "categories" + '/' + $scope.select.uuid + '/' + "has" + '/' + 'blogs' + '/' + old_obj.uuid;
				var options = {
					method: 'POST',
					endpoint: endpoint
				};
				client.request(options, function(err, data) {
					if (err) {
						$('#working1').hide();
						$scope.openAlert1("error", "", "Could not create relationship.");
					} else {
						$('#working1').hide();
						$scope.init($scope.options);
						$scope.openAlert("success", "", $rootScope.UPDATE_SUCCESS);
						$("#editBlog").animate({
							"top": "-50%"
						}, 500, function() {
							$('.modal-backdrop').remove();
						});
					}
				});
			} // end else
		}); // end client.request
	}; //end function update
	//Search Function
	//----------SEARCH----------------
    $scope.changeSelect = function(type) {
        if (type == "created") {
            $('#from_created').val('');
            $('#to_created').val('');
            $scope.formSearch.from_created.$error.pattern = false;
            $scope.formSearch.to_created.$error.pattern = false;
        } else {
            $('#from_modified').val('');
            $('#to_modified').val('');
            $scope.formSearch.from_modified.$error.pattern = false;
            $scope.formSearch.to_modified.$error.pattern = false;
        }
    }

    $scope.resetSearch = function() {
    	$scope.category = '';
        $scope.titles = '';	
        $scope.status_selected = '';
        $scope.option_1 = "";
        $scope.option_2 = "";
        $('#from_created').val('');
        $('#to_created').val('');
        $scope.formSearch.from_created.$error.pattern = false;
        $scope.formSearch.to_created.$error.pattern = false;
        $('#from_modified').val('');
        $('#to_modified').val('');
        $scope.formSearch.from_modified.$error.pattern = false;
        $scope.formSearch.to_modified.$error.pattern = false;

    }

    function convertSearchTitle(field, str) {
        var str = str.trim();
        str = str.replace(/\s+/g, " ");
        var array = str.split(" ");
        var query = '';
        if (array.length == 1) {
            query = (field + ' contains ' + "\'" + array[0] + "*\'").toString();
            query += ' OR ' + field + " = '" + str +"'";
        } else {
            for (var i = 0; i < array.length; i++) {
                query += (field + ' contains ' + "\'" + array[i] + "*\'" + ' AND ').toString();
            }
            query = query.substring(0, query.lastIndexOf('AND'));
            query += ' OR ' + field + " = '" + str +"'";
        }
        return query;
    }

    $scope.createQuery = function(titles,category, paths, option_1, from_created, to_created, option_2, from_modified, to_modified) {
        var query = "";
        var queryPath = "status = '" + paths + "'";
        if (titles != "") {
            query += convertSearchTitle("title", titles) + " AND ";
        }
        if (paths != "") {
            query += queryPath + " AND ";
        }            
         if (category != "" && category != null) {
         	var queryCate = "categoryName = '" + category.title + "'"; 
             query += queryCate + " AND ";
         }

        if (option_1 != "") {
            if (option_1 == "between") {
                if (from_created != "" && to_created != "") {
                    query += " created >= " + from_created + " AND created <= " + to_created + " AND ";
                } else {
                    //return "";
                }
            } else {
                if (from_created != "") {
                    if(option_1 != "="){
                        query += " created " + option_1 + from_created + " AND ";
                    }else{
                     var from_created2 = Date.parse($("#from_created").val() + ' 23:59:59');
                        query += " created >= " + from_created + " AND created <= " + from_created2 + " AND ";
                    }
                } else {
                   // return "";
                }
            }
        }

        if (option_2 != "") {
            if (option_2 == "between") {
                if (from_modified != "" && to_modified != "") {
                    query += " modified >= " + from_modified + " AND modified <= " + to_modified + " AND ";
                } else {
                   //return "";
                }
            } else {
                if (from_modified != "") {
                    if(option_2 != "="){
                        query += " modified " + option_2 + from_modified + " AND ";
                    }else{
                     var from_modified2 = Date.parse($("#from_modified").val() + ' 23:59:59');
                        query += " modified >= " + from_modified + " AND modified <= " + from_modified2 + " AND ";
                    }
                } else {
                    //return "";
                }
            }
        }
        query = query.substring(0, query.lastIndexOf('AND'));

        return query;
    }

    $scope.option_2 = "";
    $scope.option_1 = "";
    $scope.titles = "";

    $scope.searchData = function(collection) {
        
        var from_created = '';
        var from_modified = '';
        var to_created = '';
        var to_modified = '';

        if ($scope.option_1 == "<=") {
            from_created = Date.parse($("#from_created").val() + ' 23:59:59');
        } else {
            if ($("#from_created").val() != "" && $("#from_created").val() != undefined) {
                from_created = Date.parse($("#from_created").val());
            }            
        }
        
        if ($scope.option_2 == "<=") {
            from_modified = Date.parse($("#from_modified").val() + ' 23:59:59');
        } else {
            if ($("#from_modified").val() != "" && $("#from_modified").val() != undefined) {
                from_modified = Date.parse($("#from_modified").val());
            }
        }
        
        if ($("#to_created").val() != "" && $("#to_created").val() != undefined) {
            to_created = Date.parse($("#to_created").val() + ' 23:59:59');
        }
        
        if ($("#to_modified").val() != "" && $("#to_modified").val() != undefined) {
            to_modified = Date.parse($("#to_modified").val() + ' 23:59:59');
        }
        if ($scope.titles != "" || ($scope.category != "" && $scope.category != null) || $scope.status_selected != "" || $scope.option_1 != "" || $scope.option_2 != "" || from_created != "" || from_modified != "" || to_created != "" || to_modified != "") {
            $('#messNotFound').css("display", "none");
            var query = $scope.createQuery($scope.titles,$scope.category, $scope.status_selected, $scope.option_1, from_created, to_created, $scope.option_2, from_modified, to_modified);
            console.log(query);
            if(query != ""){
                $scope.contents = {};
                var options = {
                    method: 'GET',
                    type: collection,
                    qs: {
                        ql: query
                    } //qs:{ql:"title contains 'f*'"}
                };
                $scope.init(options);
            }else{
                $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
            }
            
        } else {
            $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
        }
    }

	$scope.reset = function() {
		$scope.init($scope.options);
		step();
		$scope.changeSelect();
	}

	function step() {
		$('table').show();
		$('.table_data p').remove();

	}
	$(document).ready(function(){
		getCategory();
	})
}
// }// end BlogController