var imageDeta = null;
var storeDeta = null;
//Images CONTROLLER

function ImagesController($scope, $rootScope) {

    document.title = 'Gallery Management';
    $scope.galleries = [];
    $scope.title = "";
    $scope.name = "";
    $scope.status = "draft";
    $scope.created = "";
    $scope.modified = "";
    $scope.description = "";
    $scope.path = "";
    $scope.storeName = "";
    $scope.select_1 = "";
    $scope.gallery = {};
    var old_gallegyuuid = "";
    var old_gallegy = "";
    $scope.selected = {};
    $scope.status_selected = "";
    $rootScope.header_title ="Gallery Management";
    $scope.str = "";
    $scope.options = {
        type: "galleries",
        qs: {
            limit: 10
        }
    };
    var cutText = function(){

         $(".comment").shorten({
            "showChars" : 30,
            "moreText"  : "more",
            "lessText"  : "less",
        });
    }
    $scope.init = function(options) {
        $('#myModal').show(); // Show modal .............
        $('#messNotFound').css("display", "none");
        $('input.checkAll').prop('checked', false);
        $scope.select_1 = "";
        $scope.galleries = [];
        var data;
        client.createCollection(options, function(err, data) {
            $('#table').css("display", "");
            $scope.checkAll(false);
            if (err) {
                $scope.openAlert("error", "", data.error_description + ".");
                $('#myModal').hide();
            } else {
                if (data._list.length <= 0) {
                    $('#messNotFound').css("display", "block");
                }
                $scope.drawPages2 = function() {
                    data.fetch(function(err, d) {
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.galleries = data._list;
                        $scope.$apply($scope.galleries);

                        if (data.hasNextPage())
                            $('#nextPage').show();
                        if (data.hasPreviousPage())
                            $('#prevPage').show();
                        cutText();
                        $('#myModal').hide();
                        $scope.checkAll(false);
                        $('#span3').height($('#span9').height());
                    });
                } //End drawUser function

                $scope.prevPage = function() {
                    data.getPreviousPage(function(err, data) {
                        if (err)
                            alert(data.error_description);
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function() {
                    data.getNextPage(function(err, data) {
                        if (err)
                            alert(data.error_description);
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            }; //End else
        });
    };

    $scope.search = '';

    $scope.check = true;
    $scope.checkAll = function(data) {
        if (data)
            $('input.chk').prop('checked', true);
        else
            $('input.chk').prop('checked', false);
    }
    $scope.check1 = function() {
        var d = false;
        $("input.chk:not(:checked)").each(function() {
            d = true;
        });
        if (d) {
            $('input.checkAll').prop('checked', false);
            $scope.check = true;
        } else {
            $('input.checkAll').prop('checked', true);
            $scope.check = false;
        }
    }

    //"Create new user" pop-up appears
    $scope.open = function() {
        $('#openDarft').prop('checked',true);
        $('#alertMsg1').css("display","none");
        $scope.gallery=[];
        $scope.gallery.title = "";
        $scope.gallery.status ="draft";
        $scope.gallery.description = "";
        $scope.gallery.path = "";
        $scope.selected = null;
        $scope.formAdd.title.$pristine = true;
        $scope.formAdd.title.$invalid = false;
        $scope.formAdd.title.$dirty = false;
        $scope.image = null;
        $('#inputImage').val('');
        getStore();
    };

    function getStore() {
        var options = {
            method: "GET",
            endpoint: "stores/",
            qs: {
                ql: "select uuid, name, title"
            }
        }

        client.request(options, function(err, data) {
            $scope.storeList = [];
            if (err)
                $scope.openAlert("error", "", data.error_description + ".");
            else {
                for (var i = 0; i < data.list.length; i++) {
                    var obj = {};
                    obj.uuid = data.list[i][0];
                    obj.name = data.list[i][1];
                    obj.title = data.list[i][2];
                    $scope.storeList.push(obj);
                };
                $scope.$apply($scope.storeList);
            }
        });
    }

    $scope.close = function() {
        location.reload();
    };

    $(document).on("keyup", "#description", function() {
        $("#description").trigger("change");
    });

    $(document).on("change", "#description", function(data) {
        $scope.changeForm($scope.gallery);
    });

    $scope.changeForm = function(obj) {
        obj.storeName = $scope.selected != null ? $scope.selected.name : "";
        console.log(imageDeta, obj);
        var b = angular.equals(imageDeta, obj);
        if (b) {
            $('#btn_update_gallery').attr("disabled", "disabled");
        } else {
            if(!$scope.formUpdate.title.$invalid){
                $('#btn_update_gallery').removeAttr("disabled", "disabled");
            }
            
        }
    };


    //Delete pages function
    $scope.deleteGallery = function() {
        
        var array = [];
        $(".chk:checked").each(function() {
            array.push($(this).val());
        });
        if (array.length > 0) {
            if (confirm($rootScope.DELETE_CONFIRM)) {
                $('#myModal').show();
                for (var i = 0; i < array.length; i++) {
                    var endpoint = 'galleries/' + array[i];
                    var options = {
                        method: 'DELETE',
                        endpoint: endpoint
                    };
                    client.request(options, function(err, data) {
                        console.log(data.entities[0].path);
                        $('#myModal').hide();
                        if (err)                 
                            $scope.openAlert("error", "", data.error_description + ".");
                        else {
                            // var path ="";
                            // if(data.entities[0] != null || data.entities[0] != undefined){
                            //     path = data.entities[0].path;
                            //     $rootScope.deleteImage(path, function(){
                            //         console.log("hahahahah");
                            //     });
                            // }
                             
                            $scope.openAlert("success", "", $scope.DELETE_SUCCESS);
                            $scope.init($scope.options);
                        }
                    });
                }
            }
        } else {
            $scope.openAlert("error", "", $scope.DELETE_SELECT);
            $('html, body').animate({scrollTop: '0px'}, 800);
        }
    }

    //Create new user function
    $scope.createGallery = function(image) {
        var endpoint = ""; 
        if(image != null && image != undefined && image.file != null && image.file != undefined){
            $rootScope.upload(image, function(){
                $scope.gallery.path = $rootScope.uploadedImgSrc;
                console.log($rootScope.uploadedImgSrc);
                addGallery();
            });
        }else{
            $scope.openAlert1("error", "", "Image is a mandatory field.");
        }
        
    }

    var addGallery = function() {
        $('#workingAdd').show();
        if ($scope.selected != null) {
            endpoint = "stores/" + $scope.selected.uuid + "/has/galleries";
            $scope.storeName = $scope.selected.title;
        } else {
            endpoint = "galleries";
            $scope.storeName = "";
        }

        var options = {
            method: "POST",
            endpoint: endpoint,
            body: {
                title: $scope.gallery.title,
                name: $scope.gallery.title,
                path: $scope.gallery.path,
                description: $scope.gallery.description,
                status: $scope.gallery.status,
                storeName: $scope.storeName
            }
        };

        client.request(options, function(err, data) {
            $('#workingAdd').hide();
            if (err) {
                $scope.openAlert1("error", "", data.error_description + ".");
            } else {
                $('#myModalAdd').modal('hide');
                $scope.openAlert("success", "", $scope.CREATE_SUCCESS);
                $scope.init($scope.options);
            }
        });

    }

    var updateGallery = function() {
        var optionsA = {
            type: "galleries/" + $scope.gallery.uuid
        };

        client.createEntity(optionsA, function(error, obj) {
            if (error)
                $scope.openAlert1("error", "", data.error_description + ".");
            else {
                obj.set("title", $scope.gallery.title);
                obj.set("status", $scope.gallery.status);
                obj.set("description", $scope.gallery.description);
                obj.set("path", $scope.gallery.path);
                if ($scope.selected != null) {
                    obj.set("storeName", $scope.selected.name);
                }
                obj.save();
                var options = {
                    method: 'PUT'
                };
                client.request(options, function(err, data) {
                    if (err && client.logging) {
                        $scope.openAlert1("error", "", data.error_description + ".");
                    } else {
                        if ($scope.selected.name != $scope.gallery.storeName) {
                            var endpoint = "stores" + '/' + $scope.selected.uuid + '/' + "has" + '/' + obj.get("type") + '/' + $scope.gallery.uuid;
                            var options = {
                                method: 'POST',
                                endpoint: endpoint
                            };
                            client.request(options, function(err, data) {
                                if (err){
                                    $scope.openAlert1("error", "", data.error_description + ".");
                                }
                            });
                        } 
                    }
                    $('#working').hide();
                    $('#myModalUpdate').modal('hide');
                    $scope.openAlert("success", "", $scope.UPDATE_SUCCESS);
                    $scope.init($scope.options);
                });
            }
        });
    }
 
    $scope.galleryDetail = function(obj) {
        imageDeta = obj;
        $scope.gallery = JSON.parse(JSON.stringify(obj));
        if($scope.gallery.status == "published"){
           $('#optionPublish').prop('checked',true);
        }else{
            $('#optionDraft').prop('checked',true);
        }
        $scope.image = {};
        $scope.image.url = $scope.gallery.path;
        $('#btn_update_gallery').attr("disabled", "disabled");
        $scope.getAllStores();
    }

    $scope.getAllStores = function() {
        var options = {
            method: "GET",
            endpoint: "stores/"
        }

        client.request(options, function(err, data) {
            if (err)
                $scope.openAlert1("error", "", data.error_description + ".");
            else {
                $scope.stores = data.entities;
                $scope.selected = data.entities[0];
                $.each(data.entities, function(key, value) {
                    if (value.name === $scope.gallery.storeName) {
                        $scope.selected = data.entities[key];
                        old_gallegyuuid = data.entities[key].uuid;
                    }
                });
                $scope.$apply($scope.selected);
                $scope.$apply($scope.stores);
            }
        });
    }

    $scope.update = function(uuid, image) {
        $('#working').show();
        // delete old connection
        if ($scope.selected.name != $scope.gallery.storeName) {
            if (old_gallegy != $scope.selected.name) { // if change category
                var endpoint = 'stores/' + old_gallegyuuid + '/has/galleries/' + uuid;
                var options = {
                    method: 'DELETE',
                    endpoint: endpoint
                };
                client.request(options, function(err, data) {
                    if (err)
                        $scope.openAlert1("error", "", data.error_description + ".");
                    else {}
                });
            }
        }
        
        var endpoint = "";  
        if(image != null && image != undefined && image.file != null && image.file != undefined){
            $rootScope.upload(image, function(){
                $scope.gallery.path = $rootScope.uploadedImgSrc;
                console.log($rootScope.uploadedImgSrc);
                updateGallery();
            });
        }else{
            updateGallery();
            //$scope.openAlert1("error", "", "Image is a mandatory field.");
        }


    }

    $scope.predicate = '_data.title';
    $('.sort').click(function() {
        if ($scope.reverse) {
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9650;</span>');
        } else {
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9660;</span>');
        }
    });

    //Search    
    $scope.changeSelect = function(type) {
        if (type == "created") {
            $('#from_created').val('');
            $('#to_created').val('');
            $scope.formSearch.from_created.$error.pattern = false;
            $scope.formSearch.to_created.$error.pattern = false;
        } else {
            $('#from_modified').val('');
            $('#to_modified').val('');
            $scope.formSearch.from_modified.$error.pattern = false;
            $scope.formSearch.to_modified.$error.pattern = false;
        }
    }

    $scope.resetSearch = function() {
        $scope.str = '';
        $scope.titles = '';
        $scope.status_selected = '';
        $scope.option_1 = "";
        $scope.option_2 = "";
        $('#from_created').val('');
        $('#to_created').val('');
        $scope.formSearch.from_created.$error.pattern = false;
        $scope.formSearch.to_created.$error.pattern = false;
        $('#from_modified').val('');
        $('#to_modified').val('');
        $scope.formSearch.from_modified.$error.pattern = false;
        $scope.formSearch.to_modified.$error.pattern = false;

    }

    function convertSearchTitle(field, str) {
        var str = str.trim();
        str = str.replace(/\s+/g, " ");
        var array = str.split(" ");
        var query = '';
        if (array.length == 1) {
            query = (field + ' contains' + "\'" + array[0] + "*\'").toString();
            query += ' OR ' + field + " = '" + str +"'";
        } else {
            for (var i = 0; i < array.length; i++) {
                query += (field + ' contains ' + "\'" + array[i] + "*\'" + ' AND ').toString();
            }
            query = query.substring(0, query.lastIndexOf('AND'));
            query += ' OR ' + field + " = '" + str +"'";
        }
        return query;
    }

    $scope.createQuery = function(titles, status_selected, store, option_1, from_created, to_created, option_2, from_modified, to_modified) {
        var query = "";
        console.log(store);
        if (titles != "") {
            query += convertSearchTitle("title", titles) + " AND ";
        }
        if (status_selected != "") {
             query += " status = '" + status_selected + "' AND ";
            // if (status_selected == 'false') {
            //     query += " status = \'1\' AND ";
            // } else {
            //     query += " status = \'0\' AND ";
            // }
        }            
        if (store != "" && store != null) {
            var queryStore = "storeName = '" + store.title + "'";
            query += queryStore + " AND ";
        }
        if (option_1 != "") {
            if (option_1 == "between") {
                if (from_created != "" && to_created != "") {
                    query += " created >= " + from_created + " AND created <= " + to_created + " AND ";
                } else {
                    //return "";
                }
            } else {
                if (from_created != "") {
                    if(option_1 != "="){
                        query += " created " + option_1 + from_created + " AND ";
                    }else{
                     var from_created2 = Date.parse($("#from_created").val() + ' 23:59:59');
                        query += " created >= " + from_created + " AND created <= " + from_created2 + " AND ";
                    }
                    
                } else {
                   // return "";
                }
            }
        }

        if (option_2 != "") {
            if (option_2 == "between") {
                if (from_modified != "" && to_modified != "") {
                    query += " modified >= " + from_modified + " AND modified <= " + to_modified + " AND ";
                } else {
                   //return "";
                }
            } else {
                if (from_modified != "") {
                    if(option_2 != "="){
                        query += " modified " + option_2 + from_modified + " AND ";
                    }else{
                     var from_modified2 = Date.parse($("#from_modified").val() + ' 23:59:59');
                        query += " modified >= " + from_modified + " AND modified <= " + from_modified2 + " AND ";
                    }
                    
                } else {
                    //return "";
                }
            }
        }
        query = query.substring(0, query.lastIndexOf('AND'));

        return query;
    }

    $scope.option_2 = "";
    $scope.option_1 = "";
    $scope.titles = "";

    $scope.searchData = function(collection) {
        var from_created = '';
        var from_modified = '';
        var to_created = '';
        var to_modified = '';

        if ($scope.option_1 == "<=") {
            from_created = Date.parse($("#from_created").val() + ' 23:59:59');
        } else {
            if ($("#from_created").val() != "" && $("#from_created").val() != undefined) {
                from_created = Date.parse($("#from_created").val());
            }            
        }
        
        if ($scope.option_2 == "<=") {
            from_modified = Date.parse($("#from_modified").val() + ' 23:59:59');
        } else {
            if ($("#from_modified").val() != "" && $("#from_modified").val() != undefined) {
                from_modified = Date.parse($("#from_modified").val());
            }
        }
        
        if ($("#to_created").val() != "" && $("#to_created").val() != undefined) {
            to_created = Date.parse($("#to_created").val() + ' 23:59:59');
        }
        
        if ($("#to_modified").val() != "" && $("#to_modified").val() != undefined) {
            to_modified = Date.parse($("#to_modified").val() + ' 23:59:59');
        }

        if ($scope.titles != "" || ($scope.str != "" && $scope.str != null) || $scope.status_selected != "" || $scope.option_1 != "" || $scope.option_2 != "" || from_created != "" || from_modified != "" || to_created != "" || to_modified != "") {
            $('#messNotFound').css("display", "none");
            var query = $scope.createQuery($scope.titles, $scope.status_selected, $scope.str, $scope.option_1, from_created, to_created, $scope.option_2, from_modified, to_modified);
            console.log(query);
            
            if(query != ""){
                $scope.galleries = {};
                var options = {
                    method: 'GET',
                    type: collection,
                    qs: {
                        ql: query
                    } 
                };
                 $scope.init(options);
            }else{
                $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
            }  
        } else {
            $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
        }
    }
    
    $(document).ready(function(){
        getStore();
    })
}


// =============== Store CONTROLLER ========================== //

function StoresController($scope, $rootScope) {

    $scope.stores = [];
    $scope.title = "";
    $scope.name = "";
    $scope.created = "";
    $scope.modified = "";
    $scope.locationname = "";
    $scope.latitude = 0;
    $scope.longtitude = 0;
    document.title = 'Store Management';
    $scope.store = {};
    $scope.select_1 = "";
    $scope.select_2 = "";
    $rootScope.header_title ="Store Management";

    $scope.options = {
        type: "stores",
        qs: {
            limit: 10
        }
    };

   $scope.init = function(options) {
        $('input.checkAll').prop('checked', false);
        $('#messNotFound').css("display", "none");
        $('#myModal').show();
        $scope.select_1 = "";
        $scope.stores = [];
        var data;
        client.createCollection(options, function(err, data) {
            $('#table').css("display", "");
            if (err) {
                //showMessCom('#messStoreList', '<div class="alert alert-error">'+ data.error_description +'</div>');
                $scope.openAlert("error", "", data.error_description);
                $('#myModal').hide();
            } else {
                if (data._list.length <= 0) {
                    $('#messNotFound').css("display", "block");
                }
                $scope.drawPages2 = function() {
                    data.fetch(function(err, d) {
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.stores = data._list;
                        $scope.$apply($scope.stores);

                        if (data.hasNextPage())
                            $('#nextPage').show();
                        if (data.hasPreviousPage())
                            $('#prevPage').show();
                        $('#myModal').hide();
                        $scope.checkAll(false);
                         $('#span3').height($('#span9').height());
                    });
                } //End drawUser function

                $scope.prevPage = function() {
                    $('#myModal').show();
                    data.getPreviousPage(function(err, data) {
                        if (err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function() {
                    $('#myModal').show();
                    data.getNextPage(function(err, data) {
                        if (err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            }; //End else
        });
    };

    $scope.changeForm = function(obj) {

        var b = angular.equals(storeDeta, obj);
        if (b) {
            $('#btn_update_store').attr("disabled", "disabled");
        } else {
            if(!$scope.formUpdate.title.$invalid){
                $('#btn_update_store').removeAttr("disabled", "disabled");
            }
        }
    };

    $scope.openNew = function() {
        $scope.store = {};
        $scope.store.title = "";
        $scope.formAdd.title.$pristine = true;
        $scope.formAdd.title.$invalid = false;
        $scope.formAdd.title.$dirty = false;
    }

    $scope.search = '';
    // $scope.filterByTitle = function(data){
    //   return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
    // }

    $scope.check = true;
    $scope.checkAll = function(data) {
        if (data)
            $('input.chk').prop('checked', true);
        else
            $('input.chk').prop('checked', false);
    }
    $scope.check1 = function() {
        var d = false;
        $("input.chk:not(:checked)").each(function() {
            d = true;
        });
        if (d) {
            $('input.checkAll').prop('checked', false);
            $scope.check = true;
        } else {
            $('input.checkAll').prop('checked', true);
            $scope.check = false;
        }
    }

    $scope.cancel = function() {
        location.href = "#/admin";
    };

    //Delete pages function
    $scope.delete = function() {

        var array = [];
        $(".chk:checked").each(function() {
            array.push($(this).val());
        });
        if (array.length > 0) {
           
            if (confirm($rootScope.DELETE_CONFIRM)) {
                $('#myModal').show();
                for (var i = 0; i < array.length; i++) {
                    var endpoint = 'stores/' + array[i];
                    var options = {
                        method: 'DELETE',
                        endpoint: endpoint
                    };
                    client.request(options, function(err, data) {
                        $('#myModal').hide();
                        if (err)
                            $scope.openAlert("error", "", data.error_description);
                        else {
                            $scope.openAlert("success", "", $scope.DELETE_SUCCESS);
                            $scope.init($scope.options);
                        }
                    });
                }
            }
        } else {
            $scope.openAlert("error", "", $scope.DELETE_SELECT);
            $('html, body').animate({scrollTop: '0px'}, 800);
        }
    }

    //Create new user function
    $scope.create = function() {
        $('#workingAdd').show();
        var options = {
            method: "POST",
            endpoint: "stores",
            body: {
                title: $scope.title,
                name: $scope.title,
                latitude: $scope.latitude,
                locationname: $scope.locationname,
                longtitude: $scope.longtitude,
                description: $scope.description
            }
        };

        client.request(options, function(err, data) {
            $('#workingAdd').hide();
            if (err)
                $scope.openAlert1("error", "", data.error_description);
            else {
                $('#myModalAdd').modal('hide');
                $scope.openAlert("success", "", $scope.CREATE_SUCCESS);
                $scope.init($scope.options);
            }
        });
    }

    $scope.storeDetail = function(obj) {
        storeDeta = obj;
        $scope.store = JSON.parse(JSON.stringify(obj));
        $('#btn_update_store').attr("disabled", "disabled");
    }

    $scope.resetStore = function() {
        $scope.title = "";
        $scope.name = "";
        $scope.description = "";
        $scope.locationname = "";
        $scope.latitude = 0;
        $scope.longtitude = 0;
        hideMessbox('#messStoreAdd');
    }

    $scope.predicate = '_data.title';
    $('.sort').click(function() {
        if ($scope.reverse) {
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9650;</span>');
        } else {
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9660;</span>');
        }
    });

    $scope.update = function(uuid) {
        $('#working').show();
        var options = {
            method: 'PUT',
            endpoint: 'stores/' + uuid,
            body: {
                title: $scope.store.title,
                name: $scope.store.name,
                locationname: $scope.store.locationname,
                latitude: $scope.store.latitude,
                description: $scope.store.description,
                longtitude: $scope.store.longtitude
            }
        };
        client.request(options, function(err, data) {
            $('#working').hide();
            if (err)
                $scope.openAlert1("error", "", data.error_description);
            else {
                $('#myModalUpdate').modal('hide');
                $scope.openAlert("success", "", $scope.UPDATE_SUCCESS);
                $scope.init($scope.options);
            }
        });
    }
    //search
    $scope.changeSelect = function(type) {
        if (type == "created") {
            $('#from_created').val('');
            $('#to_created').val('');
            $scope.formSearch.from_created.$error.pattern = false;
            $scope.formSearch.to_created.$error.pattern = false;
        } else {
            $('#from_modified').val('');
            $('#to_modified').val('');
            $scope.formSearch.from_modified.$error.pattern = false;
            $scope.formSearch.to_modified.$error.pattern = false;
        }
    }

    $scope.resetSearch = function() {
        $scope.title = '';
        $scope.locationname = '';
        $scope.option_1 = "";
        $scope.option_2 = "";
        $scope.saveStore = "--Select--"
        $('#from_created').val('');
        $('#to_created').val('');
        $scope.formSearch.from_created.$error.pattern = false;
        $scope.formSearch.to_created.$error.pattern = false;
        $('#from_modified').val('');
        $('#to_modified').val('');
        $scope.formSearch.from_modified.$error.pattern = false;
        $scope.formSearch.to_modified.$error.pattern = false;

    }

    function convertSearchTitle(field, str) {
        var str = str.trim();
        str = str.replace(/\s+/g, " ");
        var array = str.split(" ");
        var query = '';
        if (array.length == 1) {
            query = (field + ' contains ' + "\'" + array[0] + "*\'").toString();
            query += ' OR ' + field + " = '" + str +"'";
        } else {
            for (var i = 0; i < array.length; i++) {
                query += (field + ' contains ' + "\'" + array[i] + "*\'" + ' AND ').toString();
            }
            query = query.substring(0, query.lastIndexOf('AND'));
            query += ' OR ' + field + " = '" + str +"'";
        }
        return query;
    }

    $scope.createQuery = function(title, locationname, option_1, from_created, to_created, option_2, from_modified, to_modified) {
        var query = "";

        if (title != "") {
            query += convertSearchTitle("title", title) + " AND ";
        }
        if (locationname != "") {
            query += convertSearchTitle("locationname", locationname) + " AND ";
        }
        if (option_1 != "") {
            if (option_1 == "between") {
                if (from_created != "" && to_created != "") {
                    query += " created >= " + from_created + " AND created <= " + to_created + " AND ";
                } else {
                    //return "";
                }
            } else {
                if (from_created != "") {
                    if(option_1 != "="){
                        query += " created " + option_1 + from_created + " AND ";
                    }else{
                     var from_created2 = Date.parse($("#from_created").val() + ' 23:59:59');
                        query += " created >= " + from_created + " AND created <= " + from_created2 + " AND ";
                    }
                } else {
                   // return "";
                }
            }
        }

        if (option_2 != "") {
            if (option_2 == "between") {
                if (from_modified != "" && to_modified != "") {
                    query += " modified >= " + from_modified + " AND modified <= " + to_modified + " AND ";
                } else {
                   //return "";
                }
            } else {
                if (from_modified != "") {
                    if(option_2 != "="){
                        query += " modified " + option_2 + from_modified + " AND ";
                    }else{
                     var from_modified2 = Date.parse($("#from_modified").val() + ' 23:59:59');
                        query += " modified >= " + from_modified + " AND modified <= " + from_modified2 + " AND ";
                    }
                } else {
                    //return "";
                }
            }
        }
        query = query.substring(0, query.lastIndexOf('AND'));

        return query;
    }

    $scope.option_2 = "";
    $scope.option_1 = "";

    $scope.searchData = function(collection) {
        var from_created = '';
        var from_modified = '';
        var to_created = '';
        var to_modified = '';

        if ($scope.option_1 == "<=") {
            from_created = Date.parse($("#from_created").val() + ' 23:59:59');
        } else {
            if ($("#from_created").val() != "" && $("#from_created").val() != undefined) {
                from_created = Date.parse($("#from_created").val());
            }            
        }
        
        if ($scope.option_2 == "<=") {
            from_modified = Date.parse($("#from_modified").val() + ' 23:59:59');
        } else {
            if ($("#from_modified").val() != "" && $("#from_modified").val() != undefined) {
                from_modified = Date.parse($("#from_modified").val());
            }
        }
        
        if ($("#to_created").val() != "" && $("#to_created").val() != undefined) {
            to_created = Date.parse($("#to_created").val() + ' 23:59:59');
        }
        
        if ($("#to_modified").val() != "" && $("#to_modified").val() != undefined) {
            to_modified = Date.parse($("#to_modified").val() + ' 23:59:59');
        }

        if ($scope.title != "" || $scope.locationname != "" || $scope.option_1 != "" || $scope.option_2 != "" || from_created != "" || from_modified != "" || to_created != "" || to_modified != "") {
            $('#messNotFound').css("display", "none");
            var query = $scope.createQuery($scope.title, $scope.locationname, $scope.option_1, from_created, to_created, $scope.option_2, from_modified, to_modified);
            console.log("query =" + query);
            if(query != ""){
                $('#workingSearch').show();
                $scope.stores = {};
                var options = {
                    method: 'GET',
                    type: collection,
                    qs: {
                        ql: query
                    } 
                };
                 $scope.init(options);
                // client.createCollection(options, function(err, data) {
                //     if (err) {
                //         $('#workingSearch').hide();
                //         $scope.openAlert("error", "", data.error_description);
                //     } else {
                //         $scope.stores = data._list;
                //         $scope.$apply($scope.stores);
                //         $('#workingSearch').hide();
                //         if (data._list.length <= 0) {
                //             $('#messNotFound').css("display", "block");
                //         }
                //     }
                // });
            }else{
                $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
            }
            
        } else {
            $scope.openAlert("error", "", "Please enter the search criteria to perform search.");
        }
    }

    
}
// function hide message box
hideMessbox = function(element) {
    $(element).css("display", "none");
}
    

// ========== End Store list controller
