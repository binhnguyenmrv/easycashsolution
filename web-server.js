
/**
 * Module dependencies.
 */
var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , crypto = require('crypto')
  , fs = require('fs')
  , path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 6789);
app.set('views', __dirname + '/app');
app.engine('html', require('ejs').renderFile);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.cookieSession({secret: 'This is my secret'}));
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/app'));
app.use(express.static(path.join(__dirname, 'app')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);

// app.post('/delete', function(req, res) {
//   console.log("hahahah = " + req.files , req.path);
//     fs.unlink('upload/4.jpg', function (err) {
//       console.log('successfully deleted /tmp/hello');
//   });   
// });

app.post('/upload', function(req, res) {
     var image =  req.files.image;
  // console.log(req.files);
    var newImageLocation = path.join(__dirname, 'app/upload', image.name);
    console.log(''+newImageLocation);
    fs.readFile(image.path, function(err, data) {
        fs.writeFile(newImageLocation, data, function(err) {
            res.json(200, { 
                src: 'upload/' + image.name,
                size: image.size
            });
        });
    });
});



http.createServer(app).listen(6789);
console.log("6789 is started");